package nl.hanze.movieshowtime.processor.utils;



import org.apache.commons.lang3.StringUtils;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is written to create a custom library whit some shortcut methods
 *  and som other tools
 */
public class Utils
{

    private static String OS = System.getProperty("os.name").toLowerCase();



    /**
     * check os is windows or not
     * @return boolean
     */
    public static boolean isWindows() {
        return (OS.contains("win"));
    }

    /**
     * check mac is windows or not
     * @return boolean
     */
    public static boolean isMac() {
        return (OS.contains("mac"));
    }

    /**
     * check mac os unix or not
     * @return boolean
     */
    public static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix") );
    }

    /**
     * check mac os solaris or not
     * @return boolean
     */
    public static boolean isSolaris() {
        return (OS.contains("sunos"));
    }


    public static String pathToForwardSleshes(String path){
        return path.replace("\\", "/");
    }

    public static String path(String path) {
        if(!Utils.isWindows()){
            path = path.replace("\\", "/");
        }else{
            path = path.replace("\\\\", "\\");
        }
        return path;
    }


    public static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static String getCurrentTime(){
        return Utils.date("HH:mm:ss");
    }


    public static String getCurrentDate(){
        return Utils.date("dd-MM-yyyy");
    }



    public static String currentDateTime(){
        return Utils.getCurrentDate()+" ("+ Utils.getCurrentTime()+")";
    }

    public static String getCurrentTimeForFile(){
        return Utils.date("dd-MM-yyyy_HH.mm");
    }


    public static String date(String date){
        DateFormat dateFormat = new SimpleDateFormat(date);
        return dateFormat.format(new Date());
    }

    public static void sleep(int time){
        try{
            Thread.currentThread().sleep(time);
        }catch(Exception ie){
            ie.printStackTrace();
       }
    }

    /**
     * method will save the string in clipbord memory
     * @param str
     */
    public static void toClipboard(String str){
        if(!str.isEmpty()){
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Clipboard clipboard = toolkit.getSystemClipboard();
            StringSelection strSel = new StringSelection(str);
            clipboard.setContents(strSel, null);
            System.out.println("The information is paste to clipboard...");
        }else{
            System.out.println("Notice: String was empty, noting is paste to clipboard");
        }
    }


	public static boolean isNumeric(String str){
		return str.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+");
	}

	/**
	 * check if a int array is empty or not
	 * @param arr int[]
	 * @return boolean
	 */
	public static boolean isEmpty(int[] arr){
		int i=0;
		boolean isEmpty=true;
		while(i < arr.length && isEmpty){
			if(arr[i] > 0){
				isEmpty = false;
			}
			i++;
		}
		return isEmpty;
	}


    /**
     * fix path by adding a suffix /
     * @param path String
     * @return path
     */
    public static String fixPath(String path, String ds){
        int size = path.length();
        if(!path.substring(size-1, size).equals(ds)){
            path = path+ds;
        }
        return path;
    }


    /**
     * make a repeated formated string
     * @param prefix  first part string
     * @param suffix  last part string
     * @param repeat  amount of space to repeat
     * @return String
     */
    public static String line(String prefix, String suffix, int repeat){
        String  str = " ";
        String repeatedStr = StringUtils.repeat(str, repeat - prefix.length());
        return prefix+repeatedStr+suffix;
    }
}
