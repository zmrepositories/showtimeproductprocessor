package nl.hanze.movieshowtime.processor.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;

public class Log
{
    static final Logger logger = Logger.getLogger(Log.class);
    private static Log instance = null;

    // create dir if not exist
    protected Log() {
        PropertyConfigurator.configure("./config/log4j.properties");
    }
    public synchronized static Log getInstance() {
        if(instance == null) {
            instance = new Log();
        }
        return instance;
    }


    public void info(String message)
    {
        logger.info(message);
    }

    public void warn(String message)
    {
        logger.warn(message);
    }

    public void debug(String message)
    {
        logger.debug(message);
    }

    public void error(String message)
    {
        logger.error(message);
    }

    public void fatal(String message)
    {
        logger.fatal(message);
    }

    public void notice(String message)
    {
        logger.log(NoticeLevel.NOTICE, message);
    }


    public void success(String message)
    {
        logger.log(SuccessLevel.SUCCESS, message);
    }


    public static void main(String[] args) throws IOException {

        Log.getInstance().debug("Sample debug DayException");

        Log.getInstance().info("Sample info DayException");

        Log.getInstance().warn("Sample warn DayException");

        Log.getInstance().error("Sample error DayException");

        Log.getInstance().fatal("Sample fatal DayException");

        Log.getInstance().notice("Sample notice (custom) DayException");

        Log.getInstance().success("Sample success (custom) DayException");


    }
}
