package nl.hanze.movieshowtime.processor.utils;

import org.apache.log4j.Level;



// Referenced classes of package org.apache.log4j:
//            Priority

public class SuccessLevel extends Level
{
    public static final SuccessLevel SUCCESS = new SuccessLevel(20000, "SUCCESS",0);

    public SuccessLevel(int level, String levelStr, int syslogEquivalent)
    {
        super(level, levelStr, syslogEquivalent);
    }

    public static SuccessLevel toLevel(int val, Level defaultLevel)
    {
        return SUCCESS;
    }

    public static SuccessLevel toLevel(String sArg, Level defaultLevel)
    {
        return SUCCESS;
    }
}
