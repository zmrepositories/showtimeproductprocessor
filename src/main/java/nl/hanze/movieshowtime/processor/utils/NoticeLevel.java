package nl.hanze.movieshowtime.processor.utils;

import org.apache.log4j.Level;


// Referenced classes of package org.apache.log4j:
//            Priority

public class NoticeLevel extends Level
{
    public static final NoticeLevel NOTICE = new NoticeLevel(20000, "NOTICE",0);

    public NoticeLevel(int level, String levelStr, int syslogEquivalent)
    {
        super(level, levelStr, syslogEquivalent);
    }

    public static NoticeLevel toLevel(int val, Level defaultLevel)
    {
        return NOTICE;
    }

    public static NoticeLevel toLevel(String sArg, Level defaultLevel)
    {
        return NOTICE;
    }
}
