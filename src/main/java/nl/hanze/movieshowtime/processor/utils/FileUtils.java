package nl.hanze.movieshowtime.processor.utils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileUtils {
    public static void copy( File source, File destination ) throws IOException {
        if( source.isDirectory() ) {
            copyDirectory( source, destination );
        } else {
            copyFile( source, destination );
        }
    }

    public static void copyDirectory( File source, File destination ) throws IOException {
        if( !source.isDirectory() ) {
            throw new IllegalArgumentException( "Source (" + source.getPath() + ") must be a directory." );
        }

        if( !source.exists() ) {
            throw new IllegalArgumentException( "Source directory (" + source.getPath() + ") doesn't exist." );
        }

        if( destination.exists() ) {
            throw new IllegalArgumentException( "Destination (" + destination.getPath() + ") exists." );
        }
        destination.mkdirs();
        File[] files = source.listFiles();

        for( File file : files ) {
            if( file.isDirectory() ) {
                copyDirectory( file, new File( destination, file.getName() ) );
            } else {
                copyFile( file, new File( destination, file.getName() ) );
            }
        }
    }

    public static void copyFile( File source, File destination ) throws IOException {
        FileChannel sourceChannel = new FileInputStream( source ).getChannel();
        FileChannel targetChannel = new FileOutputStream( destination ).getChannel();
        sourceChannel.transferTo(0, sourceChannel.size(), targetChannel);
        sourceChannel.close();
        targetChannel.close();
    }

    /**
     * remove files from directory
     * @param path fill path
     */
    public static void emptyDir(String path){

        File dir = new File(path);
        if (!dir.exists()) {
            System.out.println("["+path+"] does not exist");
            return;
        }
        String[] info = dir.list();
        for(int i = 0; i < info.length; i++){
            File n = new File(path + info[i]);
            if (!n.isFile()) // skip ., .., other directories too
                continue;
            System.out.println("removing: " + n.getName());
            if (!n.delete())
                System.err.println("Couldn't remove ["+ n.getPath()+"]");
        }
    }

    /**
     * get the path of the file
     * @param file Object
     * @return path
     */
    public static String getAbsoluteFilePath(File file){
        String absolutePath = file.getAbsolutePath();
        return absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));
    }
}
