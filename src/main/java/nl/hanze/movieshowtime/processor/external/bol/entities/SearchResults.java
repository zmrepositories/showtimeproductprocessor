package nl.hanze.movieshowtime.processor.external.bol.entities;


import javax.xml.bind.annotation.*;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SearchResults")
public class SearchResults
{

    @XmlAttribute(name = "xmlns")
    public String xmlns;


    @XmlElement(name = "TotalResultSize")
    public long totalResultSize;



    @XmlElement(name = "Products")
    private List<Product> products;

    /**
     * get the result size if de bol response
     *
     * @return long
     */
    public long getToTalResultSize()
    {
        return totalResultSize;
    }


    /**
     * get the size of the amount of products
     *
     * @return int size
     */
    public int getSize()
    {
        return (products != null)?products.size():0;
    }


    /**
     * give back all the products
     *
     * @return List<Product>
     */
    public List<Product> getProducts()
    {
        return products;
    }


    public String toString()
    {
        String out = "SearchResults: \n";
        if(getSize() > 0){
            for(Product product : getProducts())
            {
                out += product.toString();
            }
        }

        return out;
    }
}
