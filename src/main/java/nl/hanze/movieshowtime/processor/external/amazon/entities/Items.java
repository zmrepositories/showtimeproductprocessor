package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "request",
        "totalResults",
        "totalPages",
        "moreSearchResultsUrl",
        "item"
})
public class Items {

    @XmlElement(name = "Request", required = true)
    protected Request request;
    @XmlElement(name = "TotalResults")
    protected byte totalResults;
    @XmlElement(name = "TotalPages")
    protected byte totalPages;
    @XmlElement(name = "MoreSearchResultsUrl", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String moreSearchResultsUrl;
    @XmlElement(name = "Item")
    protected List<Item> item;

    /**
     * Gets the value of the request property.
     *
     * @return
     *     possible object is
     *     {@link Request }
     *
     */
    public Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     *
     * @param value
     *     allowed object is
     *     {@link Request }
     *
     */
    public void setRequest(Request value) {
        this.request = value;
    }

    /**
     * Gets the value of the totalResults property.
     *
     */
    public byte getTotalResults() {
        return totalResults;
    }

    /**
     * Sets the value of the totalResults property.
     *
     */
    public void setTotalResults(byte value) {
        this.totalResults = value;
    }

    /**
     * Gets the value of the totalPages property.
     *
     */
    public byte getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     *
     */
    public void setTotalPages(byte value) {
        this.totalPages = value;
    }

    /**
     * Gets the value of the moreSearchResultsUrl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMoreSearchResultsUrl() {
        return moreSearchResultsUrl;
    }

    /**
     * Sets the value of the moreSearchResultsUrl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMoreSearchResultsUrl(String value) {
        this.moreSearchResultsUrl = value;
    }

    /**
     * Gets the value of the item property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Item }
     *
     *
     */
    public List<Item> getItem() {
        if (item == null) {
            item = new ArrayList<Item>();
        }
        return this.item;
    }







}