package nl.hanze.movieshowtime.processor.external.amazon.entities;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EANListElement" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "eanListElement"
})
public class EANList {

    @XmlElement(name = "EANListElement")
    protected long eanListElement;

    /**
     * Gets the value of the eanListElement property.
     *
     */
    public long getEANListElement() {
        return eanListElement;
    }

    /**
     * Sets the value of the eanListElement property.
     *
     */
    public void setEANListElement(long value) {
        this.eanListElement = value;
    }

}