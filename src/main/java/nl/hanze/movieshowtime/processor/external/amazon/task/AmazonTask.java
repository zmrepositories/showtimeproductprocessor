package nl.hanze.movieshowtime.processor.external.amazon.task;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.client.ShowTimeClient;
import nl.hanze.movieshowtime.entities.ProductCollection;
import nl.hanze.movieshowtime.processor.external.amazon.entities.Item;
import nl.hanze.movieshowtime.processor.external.amazon.entities.ItemSearchResponse;
import nl.hanze.movieshowtime.processor.scheduler.Scheduler;
import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;
import nl.hanze.movieshowtime.processor.utils.Log;

import javax.ws.rs.core.MediaType;

/**
 * Created by mickey on 25-1-2015.
 */
public class AmazonTask extends TimerTask implements Task
{

	/**
	 * stored events for this task
	 */
	private List<TaskListener> listeners = new ArrayList<TaskListener>();

	/**
	 * the execute or run increment number stored the count of executions
	 */
	private long increment = 0L;

	/**
	 * the name of the task
	 */
	private String name = "AMAZON.com task runner";

	/**
	 * origin default label amazon product
	 */
	private String origin = "amazon.com";

	/**
	 * default set on a interval sequence of five seconds for the task will run
	 * again
	 */
	private long intervalTime = Scheduler.ONCE_PER_TEN_SECONDS;

	/**
	 * the number of max executions default disabled and will execute till there
	 * are no product to get from de amazon.com api
	 */
	private long maxExecuteNumber = -1L;

	/**
	 * the offset the start position of the set of products/movies/dvd to get from bol
	 */
	private long offset = 1L;

	/**
	 * this is the amazon end point default setting according to the region you
	 * are
	 *
	 * @link 
	 *       http://docs.aws.amazon.com/AWSECommerceService/latest/DG/Locales.html
	 */
	private String endpoint = "ecs.amazonaws.co.uk";

	/**
	 * the amazon access key
	 *
	 * @link http://docs.aws.amazon.com/AWSSimpleQueueService/latest/
	 *       SQSGettingStartedGuide/AWSCredentials.html
	 */
	private String awsAccessKeyId = "AKIAJP33LQB3NU7GACPQ";

	/**
	 * the amazon secret access key
	 *
	 * @link 
	 *       http://docs.aws.amazon.com/general/latest/gr/getting-aws-sec-creds.html
	 */
	private String awsSecretKey = "9oWboam+IrQLMmIKEfptI12I27AKO1UIhUwFtC+P";

	/**
	 * default search value it will try to get all the dvd from bol.com
	 */
	private String query = "Blu-ray, dvd";

	/**
	 * the default amazon api version
	 */
	private String version = "2013-08-01";


	/**
	 * counter for empty results
	 */
	private int countEmptyResult = 0;


	/**
	 * max trigger by empty results
	 */
	private int closeByEmptyResultMaxNumber = 10;



	private ShowTimeClient client;

	public AmazonTask() {

		this.client = new ShowTimeClient();
		// this.client.setDomainUrl("set here a different domain");
	}

	public long getIntervalTime()
	{
		return intervalTime;
	}

	/**
	 * get the interval sequence value
	 *
	 * @param intervalTime
	 *            long
	 */
	public void setIntervalTime(long intervalTime)
	{
		this.intervalTime = intervalTime;
	}

	/**
	 * increase the number of execution
	 */
	private void increaseIncrementNumber()
	{
		if (increment < Long.MAX_VALUE)
			increment++;
		else
			increment = 0;
	}

	/**
	 * give back the increment number for this task
	 *
	 * @return increment long
	 */
	public synchronized long getIncrementNumber()
	{
		return increment;
	}

	/**
	 * getter for getting maxExecuteNumber
	 *
	 * @return long
	 */
	public long getMaxExecuteNumber()
	{
		return maxExecuteNumber;
	}

	/**
	 * setter for maxExecuteNumber
	 *
	 * @param maxExecuteNumber
	 *            long
	 * @return Task
	 */
	public Task setMaxExecuteNumber(long maxExecuteNumber)
	{
		this.maxExecuteNumber = maxExecuteNumber;
		return this;
	}

	/**
	 * stop the task if the number is max execution is reached
	 */
	private synchronized void cancelByMaxExecuteNumberReached()
	{
		if (maxExecuteNumber > -1 && maxExecuteNumber >= increment) {
			cancel();
		}
	}

	/**
	 * set the name of the task
	 *
	 * @param name
	 *            name task
	 */
	public synchronized void setName(String name)
	{
		this.name = name;
	}

	/**
	 * get the name of the task
	 *
	 * @return name task
	 */
	public synchronized String getName()
	{
		return name;
	}


	/**
	 * getter for getting offset
	 *
	 * @return int
	 */
	public long getOffset()
	{
		return offset;
	}


	/**
	 *  setter for offset
	 *
	 * @param offset int
	 */
	public synchronized AmazonTask setOffset(long offset)
	{
		this.offset     = offset;
		this.increment  = offset;
		return this;
	}

	/**
	 * getter for getting version
	 *
	 * @return String
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 *  setter for version
	 *
	 * @param version String
	 * @return void
	 */
	public AmazonTask setVersion(String version)
	{
		this.version = version;
		return this;
	}

	/**
	 * getter for getting endpoint
	 *
	 * @return String
	 */
	public synchronized String getEndPoint()
	{
	    return endpoint;
	}

	/**
	 *  setter for endpoint
	 *
	 * @param endpoint String
	 * @return AmazonTask
	 */
	public AmazonTask setEndPoint(String endpoint)
	{
	    this.endpoint = endpoint;
	    return this;
	}

	/**
	 * getter for getting awsAccessKeyId
	 *
	 * @return String
	 */
	public String getAwsAccessKeyId()
	{
	    return awsAccessKeyId;
	}


	/**
	 *  setter for awsAccessKeyId
	 *
	 * @param awsAccessKeyId String
	 * @return AmazonTask
	 */
	public AmazonTask setAwsAccessKeyId(String awsAccessKeyId)
	{
	    this.awsAccessKeyId = awsAccessKeyId;
	    return this;
	}

	/**
	 * getter for getting awsSecretKey
	 *
	 * @return String
	 */
	public String getAwsSecretKey()
	{
	    return awsSecretKey;
	}


	/**
	 *  setter for awsSecretKey
	 *
	 * @param awsSecretKey String
	 * @return AmazonTask
	 */
	public AmazonTask setAwsSecretKey(String awsSecretKey)
	{
	    this.awsSecretKey = awsSecretKey;
	    return this;
	}

	/**
	 * getter for getting query
	 *
	 * @return String
	 */
	public String getQuery()
	{
		return query;
	}


	/**
	 *  setter for query
	 *
	 * @param query String
	 * @return void
	 */
	public AmazonTask setQuery(String query)
	{
		this.query = query;
		return this;
	}

	public nl.hanze.movieshowtime.processor.external.amazon.entities.Items getAmazonProducts() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException
	{
		nl.hanze.movieshowtime.processor.external.amazon.entities.Items items = null;
		try{

			URLHelper helper = new URLHelper(SignedRequestsHelper.getInstance(getEndPoint(), getAwsAccessKeyId(), getAwsSecretKey()));

			helper.setKeyWord(getQuery());
			helper.addParam("ItemPage", getOffset()+"");
			helper.addParam("Version", getVersion());

			// get a signed url from the URL helper
			String signedUrl = helper.getSignedUrl();

			Client client = Client.create();

			WebResource webResource = client.resource(signedUrl);

			ClientResponse response = webResource.accept(MediaType.APPLICATION_XML).get(ClientResponse.class);

			if (response.getStatus() != 200) {
				// TODO logger and events!...
				System.out.println("Failed : HTTP error code : " + response.getStatus());
			}

			ItemSearchResponse result = response.getEntity(ItemSearchResponse.class);
			items = result.getItems();

		}catch(Exception e){
			e.printStackTrace();
			Log.getInstance().fatal(e.getMessage());
		}


		return items;
	}


	@Override
	public void run()
	{
		try {

			cancelByMaxExecuteNumberReached();

			int statusServices = client.doPing();

			if(statusServices == 200)
			{
				List<Item> amazonProducts = getAmazonProducts().getItem();
				if(amazonProducts.size() > 0){

					ProductCollection collection = new ProductCollection();

					for (nl.hanze.movieshowtime.processor.external.amazon.entities.Item amazonProduct : amazonProducts){

						collection.add(
							new nl.hanze.movieshowtime.entities.Product()
								.setEan(amazonProduct.getItemAttributes().getEAN())
								.setProductId(amazonProduct.getASIN())
								.setTitle(amazonProduct.getItemAttributes().getTitle())
								.setGpc(amazonProduct.getItemAttributes().getProductGroup())
								.setAvailabilityCode(-1)
								.setAvailabilityDescription((amazonProduct.getAvailabilityDescription() != null) ? amazonProduct.getAvailabilityDescription() : "")
									.setCondition((amazonProduct.getCondition() != null) ? amazonProduct.getCondition() : "")
									.setPrice(amazonProduct.getPrice())
									.setOrigin(origin)
									.setYear(amazonProduct.getItemAttributes().getYear())
						);
					}

					productsAddNotify(collection, client.getResponse());

					if(client.addProducts( collection )){
						successNotify(client.getResponse(), client.getStatus());
					}else{
						failedNotify(client.getResponse(), client.getStatus());
					}


				} else {
					if(countEmptyResult <= closeByEmptyResultMaxNumber){
						emptyResultNotify();
						countEmptyResult++;
					}else{
						// kill task
						cancel();
						Log.getInstance().notice(String.format("STOP task:[%s] is stopped", getName()));
					}
				}

			}else{
				// if by a sequence of four times ping the services is not available stp the task
				if(getIncrementNumber() >=3 ){
					cancel();
					Log.getInstance().notice(String.format("STOP task:[%s] is stopped", getName()));
				}
				connectServicesFailedNotify(statusServices, (int) getIncrementNumber()+1);
			}


			increaseIncrementNumber();

			if(statusServices == 200)
			{
				setOffset( getIncrementNumber() );
			}


		}catch(Exception e){
			e.printStackTrace();
			Log.getInstance().fatal(e.getMessage());
			// skip and go further...
			increaseIncrementNumber();
			setOffset( getIncrementNumber() );
		}
	}

	/**
	 * get all the subscribed listeners
	 *
	 * @return List<TaskListener>
	 */
	public synchronized List<TaskListener> getListeners()
	{
		return listeners;
	}

	/**
	 * add a listener
	 *
	 * @param listener
	 *            TaskListener
	 */
	public synchronized void addListener(TaskListener listener)
	{
		getListeners().add(listener);
	}

	/**
	 * file all the task events
	 */
	public synchronized void emptyResultNotify()
	{
		for (TaskListener listener : getListeners()) {

			if (listener instanceof EmptyResultListener) {
				((EmptyResultListener) listener).actionPerformed(this);
			}
		}
	}

	/**
	 * file all the task events
	 */
	public synchronized void productsAddNotify(ProductCollection collection, Message response)
	{
		for (TaskListener listener : getListeners()) {

			if (listener instanceof ProductsAddListener) {
				((ProductsAddListener) listener).actionPerformed(this, collection, response);
			}
		}
	}

	/**
	 * notify on failed saving product
	 */
	public synchronized void failedNotify(Message response, int statusCode)
	{
		for (TaskListener listener : getListeners()) {
			if (listener instanceof FailedListener) {
				((FailedListener) listener).failed(this, response, statusCode);
			}
		}
	}

	/**
	 * notify on failed saving product
	 */
	public synchronized void connectServicesFailedNotify(int statusCode, int sequence)
	{
		for (TaskListener listener : getListeners()) {
			if (listener instanceof ConnectionServicesFailureListener) {
				((ConnectionServicesFailureListener) listener).failed(this, statusCode, sequence);
			}
		}
	}

	/**
	 * notify on successfully product saved
	 */
	public synchronized void successNotify(Message response, int statusCode)
	{
		for (TaskListener listener : getListeners()) {
			if (listener instanceof SuccessListener) {
				((SuccessListener) listener).success(this, response, statusCode);
			}
		}
	}

	/**
	 * get the show time client from the task
	 *
	 * @return ShowTimeClient
	 */
	public ShowTimeClient getClient()
	{
		return client;
	}
}
