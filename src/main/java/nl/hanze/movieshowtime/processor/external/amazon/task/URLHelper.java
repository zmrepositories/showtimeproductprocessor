package nl.hanze.movieshowtime.processor.external.amazon.task;

import java.util.HashMap;
import java.util.Map;

/*
 * This class makes a authenticated ItemSearch call to the
 * Amazon Product Advertising API.
 */

public class URLHelper {

	private Map<String, String> arguments;

	private SignedRequestsHelper helper;

	private String url;

	/*
	 * Your AWS Access Key ID, as taken from the AWS Your Account page.
	 */
	private String awsAccessKeyId;

	/*
	 * Your AWS Secret Key corresponding to the above ID, as taken from the AWS
	 * Your Account page.
	 */
	private String awsSecretKey;

	/*
	 * You Associate Tag from amazon
	 */
	private static final String ASSOCIATE_TAG = "school021d-20";

	/*
	 * Use one of the following end-points, according to the region you are
	 * interested in:
	 * 
	 * US: ecs.amazonaws.com CA: ecs.amazonaws.ca UK: ecs.amazonaws.co.uk DE:
	 * ecs.amazonaws.de FR: ecs.amazonaws.fr JP: ecs.amazonaws.jp
	 */
	private String endpoint;

	/*
	 * The keyword to lookup. You can choose a different value if this value
	 * does not work in the locale of your choice.
	 */
	private String keyword = null;

	public URLHelper(SignedRequestsHelper helper) {
		this.helper = helper;
		this.endpoint = helper.getEndpoint();
		this.awsAccessKeyId = helper.getAwsAccessKeyId();
		this.awsSecretKey = helper.getAwsSecretKey();

		this.arguments = new HashMap<String, String>();

		setDefaultParams();
	}

	/**
	 * get the url query back
	 *
	 * @return String url query
	 */
	public String getUrlQuery() {
		String args = "?";
		for (Map.Entry<String, String> entry : arguments.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			args += String.format("%s=%s&", key, value);
		}
		return args.substring(0, args.length() - 1);
	}

	/**
	 * getter for getting url
	 *
	 * @return String url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * setter for url
	 *
	 * @param url
	 *            url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * getter for getting keyword
	 *
	 * @return String
	 */
	public String getKeyWord() {
		return keyword;
	}

	/**
	 * setter for keyword
	 *
	 * @param keyword
	 *            String
	 */
	public void setKeyWord(String keyword) {
		this.keyword = keyword;
		arguments.put("Keywords", keyword);
	}

	/**
	 * getter for getting param
	 *
	 * @return String
	 */
	public String getParam(String key) {
		return arguments.get(key);
	}

	/**
	 * set optional params / arguments
	 *
	 * @param key
	 *            String
	 * @param argument
	 *            String
	 */
	public void addParam(String key, String argument) {
		arguments.put(key, argument);
	}

	/**
	 * set default parameters for amazon
	 */
	private void setDefaultParams() {
		arguments.put("AWSAccessKeyId", getAwsSecretKey());
		arguments.put("AssociateTag", ASSOCIATE_TAG);
		arguments.put("Merchant", "Amazon");
		arguments.put("Operation", "ItemSearch");
		arguments.put("ResponseGroup", "Large");
		arguments.put("SearchIndex", "Video");
		arguments.put("Service", "AWSECommerceService");
		arguments.put("Availability", "Available");
		arguments.put("Condition", "All");
	}

	/**
	 * get a signed url with values from hasmap arguments as set in
	 * setDefaultParams
	 */
	public String getSignedUrl() {
		System.out.println(arguments);
		setUrl(helper.sign(arguments));
		return getUrl();
	}

	/**
	 * @return the basic endpoint for amazon api
	 */

	public String getEndpoint() {
		return endpoint;
	}

	/**
	 * @return the setted access key
	 */
	public String getAwsAccessKeyId() {
		return awsAccessKeyId;
	}

	/**
	 * @return the setted secret key
	 */
	public String getAwsSecretKey() {
		return awsSecretKey;
	}

}
