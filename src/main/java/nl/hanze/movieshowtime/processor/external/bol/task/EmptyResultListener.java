package nl.hanze.movieshowtime.processor.external.bol.task;

import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;

/**
 * @version 1.0
 * this listener will be fired if there are no product to receive from the api
 */
public interface EmptyResultListener extends TaskListener
{
    public void actionPerformed(Task task);
}
