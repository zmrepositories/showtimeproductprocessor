package nl.hanze.movieshowtime.processor.external.bol.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;


@XmlRootElement(name="Offers")
public class Offer
{

    private long id;


    private BigDecimal price;


    private BigDecimal listPrice;


    private String condition;


    private int availabilityCode;


    private String availabilityDescription;


    private Seller seller;


    public long getId()
    {
        return id;
    }


    @XmlElement(name = "Id")
    public Offer setId(long id)
    {
        this.id = id;
        return this;
    }


    public BigDecimal getPrice()
    {
        return price;
    }


    @XmlElement(name = "Price")
    public Offer setPrice(BigDecimal price)
    {
        this.price = price;
        return this;
    }



    public BigDecimal getListPrice()
    {
        return listPrice;
    }


    @XmlElement(name = "ListPrice")
    public Offer setListPrice(BigDecimal listPrice)
    {
        this.listPrice = listPrice;
        return this;
    }


    public String getCondition()
    {
        return condition;
    }


    @XmlElement(name = "Condition")
    public Offer setCondition(String condition)
    {
        this.condition = condition;
        return this;
    }


    public int getAvailabilityCode()
    {
        return availabilityCode;
    }


    @XmlElement(name = "AvailabilityCode")
    public void setAvailabilityCode(int availabilityCode)
    {
        this.availabilityCode = availabilityCode;
    }

    public String getAvailabilityDescription()
    {
        return availabilityDescription;
    }


    @XmlElement(name = "AvailabilityDescription")
    public Offer setAvailabilityDescription(String availabilityDescription)
    {
        this.availabilityDescription = availabilityDescription;
        return this;
    }


    public Seller getSeller()
    {
        return seller;
    }


    @XmlElement(name = "Seller")
    public Offer setSeller(Seller seller)
    {
        this.seller = seller;
        return this;
    }


    public String toString()
    {
        return String.format("\n\t - Offer: [\n " +
                "\t\t id: %s " +
                "\n\t\t price: %s "+
                "\n\t\t list price: %s "+
                "\n\t\t condition: %s "+
                "\n\t\t availability code: %s " +
                "\n\t\t availability description: %s " +
                "%s"+
                "\n\t]"
                ,getId(), getPrice(), getListPrice(), getCondition(), getAvailabilityCode(), getAvailabilityDescription(), getSeller());
    }
}
