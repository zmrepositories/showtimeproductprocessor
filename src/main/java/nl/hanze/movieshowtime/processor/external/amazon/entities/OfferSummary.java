package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name="OfferSummary")
public class OfferSummary
{

    @XmlElement(name = "LowestNewPrice")
    protected LowestNewPrice lowestNewPrice;

    @XmlElement(name = "LowestUsedPrice")
    protected LowestUsedPrice lowestUsedPrice;

    @XmlElement(name = "LowestCollectiblePrice")
    protected LowestCollectiblePrice lowestCollectiblePrice;

    @XmlElement(name = "TotalNew")
    protected int totalNew;

    @XmlElement(name = "TotalUsed")
    protected int totalUsed;

    @XmlElement(name = "TotalCollectible")
    protected int totalCollectible;

    @XmlElement(name = "TotalRefurbished")
    protected int totalRefurbished;

    /**
     * Gets the value of the lowestNewPrice property.
     *
     * @return
     *     possible object is
     *     {@link LowestNewPrice }
     *
     */
    public LowestNewPrice getLowestNewPrice() {
        return lowestNewPrice;
    }

    /**
     * Sets the value of the lowestNewPrice property.
     *
     * @param value
     *     allowed object is
     *     {@link LowestNewPrice }
     *
     */
    public void setLowestNewPrice(LowestNewPrice value) {
        this.lowestNewPrice = value;
    }

    /**
     * Gets the value of the lowestUsedPrice property.
     *
     * @return
     *     possible object is
     *     {@link LowestUsedPrice }
     *
     */
    public LowestUsedPrice getLowestUsedPrice() {
        return lowestUsedPrice;
    }

    /**
     * Sets the value of the lowestUsedPrice property.
     *
     * @param value
     *     allowed object is
     *     {@link LowestUsedPrice }
     *
     */
    public void setLowestUsedPrice(LowestUsedPrice value) {
        this.lowestUsedPrice = value;
    }

    /**
     * Gets the value of the lowestCollectiblePrice property.
     *
     * @return
     *     possible object is
     *     {@link LowestCollectiblePrice }
     *
     */
    public LowestCollectiblePrice getLowestCollectiblePrice() {
        return lowestCollectiblePrice;
    }

    /**
     * Sets the value of the lowestCollectiblePrice property.
     *
     * @param value
     *     allowed object is
     *     {@link LowestCollectiblePrice }
     *
     */
    public void setLowestCollectiblePrice(LowestCollectiblePrice value) {
        this.lowestCollectiblePrice = value;
    }

    /**
     * Gets the value of the totalNew property.
     *
     */
    public int getTotalNew() {
        return totalNew;
    }

    /**
     * Sets the value of the totalNew property.
     *
     */
    public void setTotalNew(byte value) {
        this.totalNew = value;
    }

    /**
     * Gets the value of the totalUsed property.
     *
     */
    public int getTotalUsed() {
        return totalUsed;
    }

    /**
     * Sets the value of the totalUsed property.
     *
     */
    public void setTotalUsed(byte value) {
        this.totalUsed = value;
    }

    /**
     * Gets the value of the totalCollectible property.
     *
     */
    public int getTotalCollectible() {
        return totalCollectible;
    }

    /**
     * Sets the value of the totalCollectible property.
     *
     */
    public void setTotalCollectible(byte value) {
        this.totalCollectible = value;
    }

    /**
     * Gets the value of the totalRefurbished property.
     *
     */
    public int getTotalRefurbished() {
        return totalRefurbished;
    }

    /**
     * Sets the value of the totalRefurbished property.
     *
     */
    public void setTotalRefurbished(byte value) {
        this.totalRefurbished = value;
    }




}