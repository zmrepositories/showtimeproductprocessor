package nl.hanze.movieshowtime.processor.external.bol.task;


import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.entities.Product;
import nl.hanze.movieshowtime.processor.app.ServicesSetting;
import nl.hanze.movieshowtime.processor.app.TaskInitializer;
import nl.hanze.movieshowtime.processor.app.TaskModule;
import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.utils.Log;

public class BolTaskInitializer implements TaskInitializer
{

    /**
     * stored boll task
     */
    private BolTask task;


    /**
     * stored task module with all the module configuration settings
     */
    private TaskModule module;


    /**
     * stored services setting such as url host port etc
     */
    private ServicesSetting settings;


    /**
     * constrictor set the module settings ad task reverence
     *
     * @param taskModule  TaskModule
     */
    public BolTaskInitializer(TaskModule taskModule)
    {
        this.module = taskModule;
        this.task   = (BolTask) taskModule.getTask();

    }


    @Override
    public void setServicesSettings(ServicesSetting settings)
    {
       this.settings = settings;
    }


    @Override
    public void load()
    {
        // set services url...
        task.getClient().setDomainUrl( settings.getServicesURL() );

        // set offset
        task.setOffset(module.getOffset());

        // set public key
        task.setApiKey(module.getPublicKey());

        // set max execution number
        task.setMaxExecuteNumber(module.getMaxExecuteNumber());

        // set the query value key
        task.setQuery(module.getQueryValue());

        // set api version
        task.setVersion(module.getApiVersion());



        // add events ....
        task.addListener(new EmptyResultListener() {
            @Override
            public void actionPerformed(Task task) {

                Log.getInstance().warn(String.format("Task:[%s] no more products to receive!....", task.getName()));
            }
        });


        task.addListener(new ProductAddListener()
        {
            public void actionPerformed(Task task) {}
            @Override
            public void actionPerformed(Task task, Product product, Message response) {

                System.out.println(String.format("\nINFO -- added product from offset:[%s]", task.getIncrementNumber()));
                System.out.println(product);
            }
        });



        task.addListener(new FailedListener() {
            @Override
            public void failed(Task task, Message message, Product product, int statusCode)
            {
                Log.getInstance().error(String.format("%s. %s -- %s -- ean: %s -- code: %s",((BolTask)task).getOffset(), message.getTitle(), message.getText(), product.getEan(), statusCode) );
            }

            @Override
            public void actionPerformed(Task task) {}
        });



        task.addListener(new ConnectionServicesFailureListener() {
            @Override
            public void failed(Task task, int statusCode, int sequence)
            {
                Log.getInstance().error(String.format("%s. can not connect to services:[%s] status:[%s]", sequence,  settings.getServicesURL(), statusCode));
            }

            @Override
            public void actionPerformed(Task task) {}
        });



        task.addListener(new SuccessListener() {
            @Override
            public void success(Task task, Message message, Product product, int statusCode)
            {
                Log.getInstance().success(String.format("%s. %s -- %s -- ean: %s -- code: %s", ((BolTask)task).getOffset(),  message.getTitle(), message.getText(), product.getEan(), statusCode));
            }

            @Override
            public void actionPerformed(Task task) {}
        });

    }


    @Override
    public Task getTask()
    {
        return task;
    }
}
