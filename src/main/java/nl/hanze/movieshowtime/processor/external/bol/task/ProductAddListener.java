package nl.hanze.movieshowtime.processor.external.bol.task;

import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;

/**
 * @version 1.0
 * this listener will be fired if there are no product to receive from the api
 */
public interface ProductAddListener extends TaskListener
{
    /**
     * method gives back argument current task
     *
     * @param task Task
     */
    public void actionPerformed(Task task);

    /**
     * give back arguments task and the new added product
     *
     * @param task Task
     * @param product Product
     */
    public void actionPerformed(Task task, nl.hanze.movieshowtime.entities.Product product, Message response);
}
