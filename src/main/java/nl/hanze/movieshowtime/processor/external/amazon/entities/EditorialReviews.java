package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "editorialReview"
})
public class EditorialReviews {

    @XmlElement(name = "EditorialReview")
    protected List<EditorialReview> editorialReview;

    /**
     * Gets the value of the editorialReview property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the editorialReview property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEditorialReview().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EditorialReview }
     *
     *
     */
    public List<EditorialReview> getEditorialReview() {
        if (editorialReview == null) {
            editorialReview = new ArrayList<EditorialReview>();
        }
        return this.editorialReview;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="IsLinkSuppressed" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */


}