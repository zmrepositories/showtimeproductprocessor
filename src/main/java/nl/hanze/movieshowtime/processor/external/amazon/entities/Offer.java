package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Offer")
public class Offer
{
    @XmlElement(name = "OfferListing")
    private OfferListing offerListing;

    @XmlElement(name = "OfferAttributes")
    private OfferAttributes offerAttributes;

    /**
     * getter for getting offerListing
     *
     * @return OfferListing
     */
    public OfferListing getOfferListing()
    {
        return offerListing;
    }


    /**
     *  setter for offerListing
     *
     * @param offerListing OfferListing
     * @return OfferListing
     */
    public Offer setOfferListing(OfferListing offerListing)
    {
        this.offerListing = offerListing;
        return this;
    }

    public String getAvailabilityDescription()
    {
        return (offerListing != null)? offerListing.getAvailabilityDescription() : "";
    }

    public String getCondition()
    {
        return (offerAttributes != null)? offerAttributes.getCondition() : "";
    }
}
