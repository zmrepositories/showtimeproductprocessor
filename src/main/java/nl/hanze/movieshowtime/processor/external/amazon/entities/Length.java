package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
public class Length {

    @XmlValue
    protected short value;
    @XmlAttribute(name = "Units")
    protected String units;

    /**
     * Gets the value of the value property.
     *
     */
    public short getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     */
    public void setValue(short value) {
        this.value = value;
    }

    /**
     * Gets the value of the units property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUnits() {
        return units;
    }

    /**
     * Sets the value of the units property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUnits(String value) {
        this.units = value;
    }

}