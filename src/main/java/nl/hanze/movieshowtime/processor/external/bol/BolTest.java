package nl.hanze.movieshowtime.processor.external.bol;

/**
 * Test class to call the bol.com api print out some results to the console
 */

import java.io.FileReader;
import java.io.IOException;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import nl.hanze.movieshowtime.processor.external.bol.entities.SearchResults;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class BolTest {

	/**
	 * test client with a fixed url and print out the result to the client
	 */

	public static void testClient() {
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));

		WebResource webResource = client
				.resource("https://api.bol.com/catalog/v4/search?apikey=99E07659025C43AA97F3F59099DA7426&q='dvd'&offset=300000&limit=5");

		ClientResponse response = webResource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());

		}

		SearchResults result = response.getEntity(SearchResults.class);

		System.out.println("INFO -- total result size: "
				+ result.getToTalResultSize());
		System.out.println("INFO -- amount of products: " + result.getSize());

		System.out.println(result);

	}

	/**
	 * test class to test the (un)marshaller and print out the result to the
	 * console
	 * 
	 * @throws JAXBException
	 * @throws IOException
	 */
	public static void testMarshaling() throws JAXBException, IOException {
		JAXBContext context = JAXBContext.newInstance(SearchResults.class);

		Unmarshaller unmarshaller = context.createUnmarshaller();
		SearchResults results = (SearchResults) unmarshaller
				.unmarshal(new FileReader(
						System.getProperty("user.dir")
								+ "\\src\\main\\java\\nl\\hanze\\processor\\external\\bol\\sample.xml"));

		System.out.println(results.toString());
	}

	/**
	 * main class to start up the tests
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		BolTest.testClient();
		// Test.testMarshaling();

	}

}
