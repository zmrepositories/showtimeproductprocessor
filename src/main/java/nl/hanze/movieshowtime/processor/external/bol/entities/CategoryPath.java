package nl.hanze.movieshowtime.processor.external.bol.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ParentCategoryPaths")
public class CategoryPath
{
    @XmlElement(name = "ParentCategories")
    private List<Category> categories;


    public List<Category> getCategories()
    {
        return categories;
    }





}
