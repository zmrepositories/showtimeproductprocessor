package nl.hanze.movieshowtime.processor.external.amazon.task;

import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.entities.ProductCollection;
import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;

/**
 * @version 1.0
 * this listener will be fired if there are no product to receive from the api
 */
public interface ProductsAddListener extends TaskListener
{
    /**
     * method gives back argument current task
     *
     * @param task Task
     */
    public void actionPerformed(Task task);

    /**
     * give back arguments task and the new added products
     *
     * @param task Task
     * @param collection ProductCollection
     * @param response Message
     */
    public void actionPerformed(Task task, ProductCollection collection, Message response);
}
