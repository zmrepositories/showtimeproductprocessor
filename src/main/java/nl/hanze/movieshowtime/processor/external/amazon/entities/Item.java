package nl.hanze.movieshowtime.processor.external.amazon.entities;

import nl.hanze.movieshowtime.processor.utils.Utils;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "asin",
        "detailPageURL",
        "itemLinks",
        "salesRank",
        "smallImage",
        "mediumImage",
        "largeImage",
        "imageSets",
        "itemAttributes",
        "offerSummary",
        "offers",
        "editorialReviews"
})
public class Item {

    @XmlElement(name = "ASIN", required = true)
    private String asin;

    @XmlElement(name = "DetailPageURL", required = true)
    @XmlSchemaType(name = "anyURI")
    private String detailPageURL;

    @XmlElement(name = "ItemLinks", required = true)
    private ItemLinks itemLinks;

    @XmlElement(name = "SalesRank")
    private short salesRank;

    @XmlElement(name = "SmallImage", required = true)
    private SmallImage smallImage;

    @XmlElement(name = "MediumImage", required = true)
    private MediumImage mediumImage;

    @XmlElement(name = "LargeImage", required = true)
    private LargeImage largeImage;

    @XmlElement(name = "ImageSets", required = true)
    private ImageSets imageSets;

    @XmlElement(name = "ItemAttributes", required = true)
    private ItemAttributes itemAttributes;

    @XmlElement(name = "OfferSummary")
    private OfferSummary offerSummary;

    @XmlElementWrapper(name = "Offers")
    @XmlElement(name = "Offer")
    private List<Offer> offers;

    @XmlElement(name = "EditorialReviews")
    private EditorialReviews editorialReviews;

    /**
     * Gets the value of the asin property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getASIN() {
        return asin;
    }

    /**
     * Sets the value of the asin property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setASIN(String value) {
        this.asin = value;
    }

    /**
     * Gets the value of the detailPageURL property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDetailPageURL() {
        return detailPageURL;
    }

    /**
     * Sets the value of the detailPageURL property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDetailPageURL(String value) {
        this.detailPageURL = value;
    }

    /**
     * Gets the value of the itemLinks property.
     *
     * @return
     *     possible object is
     *     {@link ItemLinks }
     *
     */
    public ItemLinks getItemLinks() {
        return itemLinks;
    }

    /**
     * Sets the value of the itemLinks property.
     *
     * @param value
     *     allowed object is
     *     {@link ItemLinks }
     *
     */
    public void setItemLinks(ItemLinks value) {
        this.itemLinks = value;
    }

    /**
     * Gets the value of the salesRank property.
     *
     */
    public short getSalesRank() {
        return salesRank;
    }

    /**
     * Sets the value of the salesRank property.
     *
     */
    public void setSalesRank(short value) {
        this.salesRank = value;
    }

    /**
     * Gets the value of the smallImage property.
     *
     * @return
     *     possible object is
     *     {@link SmallImage }
     *
     */
    public SmallImage getSmallImage() {
        return smallImage;
    }

    /**
     * Sets the value of the smallImage property.
     *
     * @param value
     *     allowed object is
     *     {@link SmallImage }
     *
     */
    public void setSmallImage(SmallImage value) {
        this.smallImage = value;
    }

    /**
     * Gets the value of the mediumImage property.
     *
     * @return
     *     possible object is
     *     {@link MediumImage }
     *
     */
    public MediumImage getMediumImage() {
        return mediumImage;
    }

    /**
     * Sets the value of the mediumImage property.
     *
     * @param value
     *     allowed object is
     *     {@link MediumImage }
     *
     */
    public void setMediumImage(MediumImage value) {
        this.mediumImage = value;
    }

    /**
     * Gets the value of the largeImage property.
     *
     * @return
     *     possible object is
     *     {@link LargeImage }
     *
     */
    public LargeImage getLargeImage() {
        return largeImage;
    }

    /**
     * Sets the value of the largeImage property.
     *
     * @param value
     *     allowed object is
     *     {@link LargeImage }
     *
     */
    public void setLargeImage(LargeImage value) {
        this.largeImage = value;
    }

    /**
     * Gets the value of the imageSets property.
     *
     * @return
     *     possible object is
     *     {@link ImageSets }
     *
     */
    public ImageSets getImageSets() {
        return imageSets;
    }

    /**
     * Sets the value of the imageSets property.
     *
     * @param value
     *     allowed object is
     *     {@link ImageSets }
     *
     */
    public void setImageSets(ImageSets value) {
        this.imageSets = value;
    }

    /**
     * Gets the value of the itemAttributes property.
     *
     * @return
     *     possible object is
     *     {@link ItemAttributes }
     *
     */
    public ItemAttributes getItemAttributes() {
        return itemAttributes;
    }

    /**
     * Sets the value of the itemAttributes property.
     *
     * @param value
     *     allowed object is
     *     {@link ItemAttributes }
     *
     */
    public void setItemAttributes(ItemAttributes value) {
        this.itemAttributes = value;
    }

    /**
     * Gets the value of the offerSummary property.
     *
     * @return
     *     possible object is
     *     {@link OfferSummary }
     *
     */
    public OfferSummary getOfferSummary() {
        return offerSummary;
    }

    /**
     * Sets the value of the offerSummary property.
     *
     * @param value
     *     allowed object is
     *     {@link OfferSummary }
     *
     */
    public void setOfferSummary(OfferSummary value) {
        this.offerSummary = value;
    }

    /**
     * Gets the value of the editorialReviews property.
     *
     * @return
     *     possible object is
     *     {@link EditorialReviews }
     *
     */
    public EditorialReviews getEditorialReviews() {
        return editorialReviews;
    }

    /**
     * Sets the value of the editorialReviews property.
     *
     * @param value
     *     allowed object is
     *     {@link EditorialReviews }
     *
     */
    public void setEditorialReviews(EditorialReviews value) {
        this.editorialReviews = value;
    }

    public List<Offer> getOffers()
    {
        return offers;
    }


    public String getAvailabilityDescription()
    {
        return (offers != null && offers.size() > 0)?offers.get(0).getAvailabilityDescription() : "";
    }

    public String getCondition()
    {
        return (offers != null && offers.size() > 0)?offers.get(0).getCondition() : "";
    }

    /**
     * get BigDecimal price back from string remove price indicator
     *
     * @return BigDecimal price
     */
    public BigDecimal getPrice()
    {
        String rowPrice = "";
        BigDecimal price = null;
        OfferSummary offer        = getOfferSummary();
        LowestNewPrice lowerPrice = offer.getLowestNewPrice();
        if(lowerPrice != null){
            rowPrice = lowerPrice.getFormattedPrice();

            if(!Utils.isNumeric(rowPrice.substring(0, 1))){
                price = new BigDecimal(rowPrice.substring(1, rowPrice.length()));
            }
        }
        return price;
    }

    /**
     * availability_code : not available
     *
     * @return String product
     */
    public String toString()
    {
        return String.format("\nAMAZON:Product: [\n " +
                        "\t product_id: %s " +
                        "\n\t ean: %s " +
                        "\n\t title: %s " +
                        "\n\t gpc: %s " +
                        "\n\t availability code: %s " +
                        "\n\t availability description: %s " +
                        "\n\t condition: %s " +
                        "\n\t price: %s " +
                        "\n\t year: %s " +
                        "\n\t origin: %s " +
                        "\n]",
                getASIN(),
                getItemAttributes().getEAN(),
                getItemAttributes().getTitle(),
                getItemAttributes().getProductGroup(),
                "-1",
                getAvailabilityDescription(),
                getCondition(),
                getPrice(),
                getItemAttributes().getYear(),
                "amazon.com"
        );
    }

}