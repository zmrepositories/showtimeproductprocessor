package nl.hanze.movieshowtime.processor.external.bol.entities;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Seller")
public class Seller
{


    private long id;


    private String type;


    private String url;


    public long getId()
    {
        return id;
    }

    @XmlElement(name = "Id")
    public Seller setId(int id)
    {
        this.id = id;
        return this;
    }


    public String getType()
    {
        return type;
    }


    @XmlElement(name = "SellerType")
    public Seller setType(String type)
    {
        this.type = type;
        return this;
    }


    public String getUrl()
    {
        return url;
    }


    @XmlElement(name = "Url")
    public Seller setUrl(String url)
    {
        this.url = url;
        return this;
    }

    public String toString()
    {
        return String.format("\n\t\t - Seller: [\n " +
                "\t\t\t id: %s " +
                "\n\t\t\t type: %s "+
                "\n\t\t\t url: %s "+
                "\n\t\t]"
                ,getId(), getType(), getUrl());
    }
}
