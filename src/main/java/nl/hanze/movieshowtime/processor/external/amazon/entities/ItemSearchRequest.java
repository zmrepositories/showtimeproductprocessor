package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Keywords" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SearchIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "keywords",
        "responseGroup",
        "searchIndex"
})
public class ItemSearchRequest {

    @XmlElement(name = "Keywords", required = true)
    protected String keywords;
    @XmlElement(name = "ResponseGroup", required = true)
    protected String responseGroup;
    @XmlElement(name = "SearchIndex", required = true)
    protected String searchIndex;

    /**
     * Gets the value of the keywords property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * Sets the value of the keywords property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setKeywords(String value) {
        this.keywords = value;
    }

    /**
     * Gets the value of the responseGroup property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResponseGroup() {
        return responseGroup;
    }

    /**
     * Sets the value of the responseGroup property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResponseGroup(String value) {
        this.responseGroup = value;
    }

    /**
     * Gets the value of the searchIndex property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSearchIndex() {
        return searchIndex;
    }

    /**
     * Sets the value of the searchIndex property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSearchIndex(String value) {
        this.searchIndex = value;
    }

}