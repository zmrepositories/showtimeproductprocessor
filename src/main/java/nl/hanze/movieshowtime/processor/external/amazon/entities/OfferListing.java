package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="OfferListing")
public class OfferListing
{

    @XmlElement(name = "Availability")
    private String availability;
    
    
    /**
     * getter for getting availabilityDescription
     *
     * @return availability
     */
    public String getAvailabilityDescription()
    {
        return availability;
    }
    
    
    /**
     *  setter for availabilityDescription
     *
     * @param availabilityDescription availability
     * @return availability
     */
    public OfferListing setAvailabilityDescription(String availabilityDescription)
    {
        this.availability = availabilityDescription;
        return this;
    }
}
