package nl.hanze.movieshowtime.processor.external.bol.task;

import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.entities.Product;
import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;

/**
 * @version 1.0
 * this listener will be fired if there are no product to receive from the api
 */
public interface SuccessListener extends TaskListener
{
    /**
     *
     * @param task Task
     * @param message Message  message response
     * @param statusCode int status code server
     */
    public void success(Task task, Message message, Product product, int statusCode);
}
