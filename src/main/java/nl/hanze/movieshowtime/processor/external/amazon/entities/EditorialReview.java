package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "source",
        "content",
        "isLinkSuppressed"
})
public class EditorialReview {

    @XmlElement(name = "Source", required = true)
    protected String source;
    @XmlElement(name = "Content", required = true)
    protected String content;
    @XmlElement(name = "IsLinkSuppressed")
    protected byte isLinkSuppressed;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the content property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the isLinkSuppressed property.
     *
     */
    public byte getIsLinkSuppressed() {
        return isLinkSuppressed;
    }

    /**
     * Sets the value of the isLinkSuppressed property.
     *
     */
    public void setIsLinkSuppressed(byte value) {
        this.isLinkSuppressed = value;
    }

}