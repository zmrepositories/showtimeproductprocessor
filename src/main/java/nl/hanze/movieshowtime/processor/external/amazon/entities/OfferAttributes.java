package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="OfferAttributes")
public class OfferAttributes
{
    @XmlElement(name = "Condition")
    private String condition;

    /**
     * getter for getting condition
     *
     * @return String
     */
    public String getCondition()
    {
        return condition;
    }


    /**
     *  setter for condition
     *
     * @param condition String
     * @return Condition
     */
    public OfferAttributes setCondition(String condition)
    {
        this.condition = condition;
        return this;
    }
}
