package nl.hanze.movieshowtime.processor.external.bol.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ParentCategories")
public class Category
{
    private int id;

    private String name;


    public int getId()
    {
        return id;
    }


    @XmlElement(name = "id")
    public Category setId(int id)
    {
        this.id = id;
        return this;
    }


    public String getName()
    {
        return name;
    }


    public boolean equals(Category category)
    {
        return category.getName().equals(getName());
    }


    @XmlElement(name = "name")
    public Category setName(String name)
    {
        this.name = name;
        return this;
    }


    public String toString()
    {
        return "\n\t\t "+String.format("Category:[id = %s, name = %s]", getId(), getName());
    }
}
