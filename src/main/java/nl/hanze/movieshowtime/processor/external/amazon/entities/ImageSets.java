package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "imageSet"
})
public class ImageSets {

    @XmlElement(name = "ImageSet")
    protected List<ImageSet> imageSet;

    /**
     * Gets the value of the imageSet property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the imageSet property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImageSet().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImageSet }
     *
     *
     */
    public List<ImageSet> getImageSet() {
        if (imageSet == null) {
            imageSet = new ArrayList<ImageSet>();
        }
        return this.imageSet;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "swatchImage",
            "smallImage",
            "thumbnailImage",
            "tinyImage",
            "mediumImage",
            "largeImage"
    })
    public static class ImageSet {

        @XmlElement(name = "SwatchImage", required = true)
        protected SwatchImage swatchImage;
        @XmlElement(name = "SmallImage", required = true)
        protected SmallImage smallImage;
        @XmlElement(name = "ThumbnailImage", required = true)
        protected ThumbnailImage thumbnailImage;
        @XmlElement(name = "TinyImage", required = true)
        protected TinyImage tinyImage;
        @XmlElement(name = "MediumImage", required = true)
        protected MediumImage mediumImage;
        @XmlElement(name = "LargeImage", required = true)
        protected LargeImage largeImage;
        @XmlAttribute(name = "Category")
        protected String category;

        /**
         * Gets the value of the swatchImage property.
         *
         * @return
         *     possible object is
         *     {@link SwatchImage }
         *
         */
        public SwatchImage getSwatchImage() {
            return swatchImage;
        }

        /**
         * Sets the value of the swatchImage property.
         *
         * @param value
         *     allowed object is
         *     {@link SwatchImage }
         *
         */
        public void setSwatchImage(SwatchImage value) {
            this.swatchImage = value;
        }

        /**
         * Gets the value of the smallImage property.
         *
         * @return
         *     possible object is
         *     {@link SmallImage }
         *
         */
        public SmallImage getSmallImage() {
            return smallImage;
        }

        /**
         * Sets the value of the smallImage property.
         *
         * @param value
         *     allowed object is
         *     {@link SmallImage }
         *
         */
        public void setSmallImage(SmallImage value) {
            this.smallImage = value;
        }

        /**
         * Gets the value of the thumbnailImage property.
         *
         * @return
         *     possible object is
         *     {@link ThumbnailImage }
         *
         */
        public ThumbnailImage getThumbnailImage() {
            return thumbnailImage;
        }

        /**
         * Sets the value of the thumbnailImage property.
         *
         * @param value
         *     allowed object is
         *     {@link ThumbnailImage }
         *
         */
        public void setThumbnailImage(ThumbnailImage value) {
            this.thumbnailImage = value;
        }

        /**
         * Gets the value of the tinyImage property.
         *
         * @return
         *     possible object is
         *     {@link TinyImage }
         *
         */
        public TinyImage getTinyImage() {
            return tinyImage;
        }

        /**
         * Sets the value of the tinyImage property.
         *
         * @param value
         *     allowed object is
         *     {@link TinyImage }
         *
         */
        public void setTinyImage(TinyImage value) {
            this.tinyImage = value;
        }

        /**
         * Gets the value of the mediumImage property.
         *
         * @return
         *     possible object is
         *     {@link MediumImage }
         *
         */
        public MediumImage getMediumImage() {
            return mediumImage;
        }

        /**
         * Sets the value of the mediumImage property.
         *
         * @param value
         *     allowed object is
         *     {@link MediumImage }
         *
         */
        public void setMediumImage(MediumImage value) {
            this.mediumImage = value;
        }

        /**
         * Gets the value of the largeImage property.
         *
         * @return
         *     possible object is
         *     {@link LargeImage }
         *
         */
        public LargeImage getLargeImage() {
            return largeImage;
        }

        /**
         * Sets the value of the largeImage property.
         *
         * @param value
         *     allowed object is
         *     {@link LargeImage }
         *
         */
        public void setLargeImage(LargeImage value) {
            this.largeImage = value;
        }

        /**
         * Gets the value of the category property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCategory() {
            return category;
        }

        /**
         * Sets the value of the category property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCategory(String value) {
            this.category = value;
        }






        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
         *         &lt;element name="Height">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>short">
         *                 &lt;attribute name="Units" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Width">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>short">
         *                 &lt;attribute name="Units" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */















    }

}