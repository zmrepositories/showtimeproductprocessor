package nl.hanze.movieshowtime.processor.external.bol.entities;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XmlRootElement(name="Products")
public class Product
{
    private long id;

    private long ean;

    private String title;

    private String gpc;

    private String summary;


    private int year;


    private OfferData offerDate;


    private List<CategoryPath> categoriesPaths = new ArrayList<CategoryPath>();


    private List<Image> images = new ArrayList<Image>();




    public long getId()
    {
        return id;
    }


    @XmlElement(name = "Id")
    public void setId(long id)
    {
        this.id = id;
    }


    public String getTitle()
    {
        return title;
    }


    @XmlElement(name = "Title")
    public Product setTitle(String title)
    {
        this.title = title;
        return this;
    }


    @XmlElement(name="OfferData")
    public Product setOfferData(OfferData offerDate)
    {
        this.offerDate = offerDate;
        return this;
    }


    public long getEAN()
    {
        return ean;
    }


    @XmlElement(name = "EAN")
    public Product setEAN(long ean)
    {
        this.ean = ean;
        return this;
    }


    public String getGPC()
    {
        return gpc;
    }


    @XmlElement(name = "GPC")
    public Product setGPC(String gpc)
    {
        this.gpc = gpc;
        return this;
    }


    public String getSummary()
    {
        return summary;
    }


    @XmlElement(name = "Summary")
    public Product setSummary(String summary)
    {
        this.summary = summary;
        setYear(subtractYear(summary));
        return this;
    }


    public Product setYear(int year)
    {
        this.year = year;
        return this;
    }


    public int subtractYear(String subject)
    {
        int year = -1;
        Pattern pattern = Pattern.compile("[0-9]{4}");
        Matcher matcher = pattern.matcher(subject);
        if (matcher.find())
        {
            year = Integer.parseInt( matcher.group(0));
        }
        return year;
    }


    public int getYear()
    {
       return year;
    }


    public Offer getOffer()
    {
        return offerDate.getOffer();
    }



    @XmlElement(name = "ParentCategoryPaths")
    public Product setCategoryPath(CategoryPath categoryPath)
    {
        this.categoriesPaths.add(categoryPath);
        return this;
    }


    public List<Category> getCategories()
    {
        CategoryList list = new CategoryList();

        for(CategoryPath path : categoriesPaths)
        {
            list.addAll(path);
        }
        return list;
    }


    @XmlElement(name = "Images")
    public Product setImage(Image img)
    {
        this.images.add(img);
        return this;
    }


    public List<Image> getImages()
    {
        return images;
    }


    public String toString()
    {


        String categories = "- Categories: [";
        for(Category category : getCategories()){
            categories += category;
        }
        categories +="\n\t]";


        String images = "- Images: [";
        for(Image img : getImages()){
            images += img;
        }
        images +="\n\t]";


        return String.format("\nBOL:Product: [\n " +
                "\t id: %s " +
                "\n\t title: %s "+
                "\n\t ean: %s "+
                "\n\t gpc: %s "+
                "\n\t year: %s " +
                "\t %s " +
                "\n\t %s"+
                "\n\t %s"+
                "\n]"
                ,getId(), getTitle(), getEAN(), getGPC(), getYear(), getOffer(), categories, images);
    }

}
