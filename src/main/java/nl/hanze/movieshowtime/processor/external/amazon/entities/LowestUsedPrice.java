package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "amount",
        "currencyCode",
        "formattedPrice"
})
public class LowestUsedPrice {

    @XmlElement(name = "Amount")
    protected short amount;
    @XmlElement(name = "CurrencyCode", required = true)
    protected String currencyCode;
    @XmlElement(name = "FormattedPrice", required = true)
    protected String formattedPrice;

    /**
     * Gets the value of the amount property.
     *
     */
    public short getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     */
    public void setAmount(short value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the formattedPrice property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFormattedPrice() {
        return formattedPrice;
    }

    /**
     * Sets the value of the formattedPrice property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFormattedPrice(String value) {
        this.formattedPrice = value;
    }

}