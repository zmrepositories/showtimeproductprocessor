package nl.hanze.movieshowtime.processor.external.amazon.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ItemAttributes")
public class ItemAttributes {

	@XmlElement(name = "Actor")
	protected List<String> actor;

	@XmlElement(name = "AspectRatio")
	protected String aspectRatio;

	@XmlElement(name = "AudienceRating", required = true)
	protected String audienceRating;

	@XmlElement(name = "Binding", required = true)
	protected String binding;

	@XmlElement(name = "Brand")
	protected String brand;

	@XmlElement(name = "Creator")
	protected List<Creator> creator;

	@XmlElement(name = "Director")
	protected List<String> director;

	@XmlElement(name = "EAN")
	protected long ean;

	@XmlElement(name = "EANList", required = true)
	protected EANList eanList;

	@XmlElement(name = "Format")
	protected String format;

	@XmlElement(name = "Label", required = true)
	protected String label;

	@XmlElement(name = "Languages")
	protected Languages languages;

	@XmlElement(name = "ListPrice")
	protected ListPrice listPrice;

	@XmlElement(name = "Manufacturer", required = true)
	protected String manufacturer;

	@XmlElement(name = "MPN")
	protected Long mpn;

	@XmlElement(name = "NumberOfDiscs")
	protected byte numberOfDiscs;

	@XmlElement(name = "NumberOfItems")
	protected byte numberOfItems;

	@XmlElement(name = "PackageDimensions")
	protected PackageDimensions packageDimensions;

	@XmlElement(name = "PackageQuantity")
	protected byte packageQuantity;

	@XmlElement(name = "PartNumber")
	protected Long partNumber;

	@XmlElement(name = "ProductGroup", required = true)
	protected String productGroup;

	@XmlElement(name = "ProductTypeName", required = true)
	protected String productTypeName;

	@XmlElement(name = "Publisher", required = true)
	protected String publisher;

	@XmlElement(name = "RegionCode")
	protected byte regionCode;

	@XmlElement(name = "ReleaseDate")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar releaseDate;

	@XmlElement(name = "RunningTime")
	protected RunningTime runningTime;

	@XmlElement(name = "Studio", required = true)
	protected String studio;

	@XmlElement(name = "Title", required = true)
	protected String title;

	/**
	 * Gets the value of the actor property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the actor property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getActor().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 *
	 *
	 */
	public List<String> getActor() {
		if (actor == null) {
			actor = new ArrayList<String>();
		}
		return this.actor;
	}

	/**
	 * Gets the value of the aspectRatio property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getAspectRatio() {
		return aspectRatio;
	}

	/**
	 * Sets the value of the aspectRatio property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setAspectRatio(String value) {
		this.aspectRatio = value;
	}

	/**
	 * Gets the value of the audienceRating property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getAudienceRating() {
		return audienceRating;
	}

	/**
	 * Sets the value of the audienceRating property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setAudienceRating(String value) {
		this.audienceRating = value;
	}

	/**
	 * Gets the value of the binding property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBinding() {
		return binding;
	}

	/**
	 * Sets the value of the binding property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setBinding(String value) {
		this.binding = value;
	}

	/**
	 * Gets the value of the brand property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * Sets the value of the brand property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setBrand(String value) {
		this.brand = value;
	}

	/**
	 * Gets the value of the creator property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the creator property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCreator().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link Creator }
	 *
	 *
	 */
	public List<Creator> getCreator() {
		if (creator == null) {
			creator = new ArrayList<Creator>();
		}
		return this.creator;
	}

	/**
	 * Gets the value of the director property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the director property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getDirector().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 *
	 *
	 */
	public List<String> getDirector() {
		if (director == null) {
			director = new ArrayList<String>();
		}
		return this.director;
	}

	/**
	 * Gets the value of the ean property.
	 *
	 */
	public long getEAN() {
		return ean;
	}

	/**
	 * Sets the value of the ean property.
	 *
	 */
	public void setEAN(long value) {
		this.ean = value;
	}

	/**
	 * Gets the value of the eanList property.
	 *
	 * @return possible object is {@link EANList }
	 *
	 */
	public EANList getEANList() {
		return eanList;
	}

	/**
	 * Sets the value of the eanList property.
	 *
	 * @param value
	 *            allowed object is {@link EANList }
	 *
	 */
	public void setEANList(EANList value) {
		this.eanList = value;
	}

	/**
	 * Gets the value of the format property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the value of the format property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setFormat(String value) {
		this.format = value;
	}

	/**
	 * Gets the value of the label property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets the value of the label property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setLabel(String value) {
		this.label = value;
	}

	/**
	 * Gets the value of the languages property.
	 *
	 * @return possible object is {@link Languages }
	 *
	 */
	public Languages getLanguages() {
		return languages;
	}

	/**
	 * Sets the value of the languages property.
	 *
	 * @param value
	 *            allowed object is {@link Languages }
	 *
	 */
	public void setLanguages(Languages value) {
		this.languages = value;
	}

	/**
	 * Gets the value of the listPrice property.
	 *
	 * @return possible object is {@link ListPrice }
	 *
	 */
	public ListPrice getListPrice() {
		return listPrice;
	}

	/**
	 * Sets the value of the listPrice property.
	 *
	 * @param value
	 *            allowed object is {@link ListPrice }
	 *
	 */
	public void setListPrice(ListPrice value) {
		this.listPrice = value;
	}

	/**
	 * Gets the value of the manufacturer property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * Sets the value of the manufacturer property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setManufacturer(String value) {
		this.manufacturer = value;
	}

	/**
	 * Gets the value of the mpn property.
	 *
	 * @return possible object is {@link Long }
	 *
	 */
	public Long getMPN() {
		return mpn;
	}

	/**
	 * Sets the value of the mpn property.
	 *
	 * @param value
	 *            allowed object is {@link Long }
	 *
	 */
	public void setMPN(Long value) {
		this.mpn = value;
	}

	/**
	 * Gets the value of the numberOfDiscs property.
	 *
	 */
	public byte getNumberOfDiscs() {
		return numberOfDiscs;
	}

	/**
	 * Sets the value of the numberOfDiscs property.
	 *
	 */
	public void setNumberOfDiscs(byte value) {
		this.numberOfDiscs = value;
	}

	/**
	 * Gets the value of the numberOfItems property.
	 *
	 */
	public byte getNumberOfItems() {
		return numberOfItems;
	}

	/**
	 * Sets the value of the numberOfItems property.
	 *
	 */
	public void setNumberOfItems(byte value) {
		this.numberOfItems = value;
	}

	/**
	 * Gets the value of the packageDimensions property.
	 *
	 * @return possible object is {@link PackageDimensions }
	 *
	 */
	public PackageDimensions getPackageDimensions() {
		return packageDimensions;
	}

	/**
	 * Sets the value of the packageDimensions property.
	 *
	 * @param value
	 *            allowed object is {@link PackageDimensions }
	 *
	 */
	public void setPackageDimensions(PackageDimensions value) {
		this.packageDimensions = value;
	}

	/**
	 * Gets the value of the packageQuantity property.
	 *
	 */
	public byte getPackageQuantity() {
		return packageQuantity;
	}

	/**
	 * Sets the value of the packageQuantity property.
	 *
	 */
	public void setPackageQuantity(byte value) {
		this.packageQuantity = value;
	}

	/**
	 * Gets the value of the partNumber property.
	 *
	 * @return possible object is {@link Long }
	 *
	 */
	public Long getPartNumber() {
		return partNumber;
	}

	/**
	 * Sets the value of the partNumber property.
	 *
	 * @param value
	 *            allowed object is {@link Long }
	 *
	 */
	public void setPartNumber(Long value) {
		this.partNumber = value;
	}

	/**
	 * Gets the value of the productGroup property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getProductGroup() {
		return productGroup;
	}

	/**
	 * Sets the value of the productGroup property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setProductGroup(String value) {
		this.productGroup = value;
	}

	/**
	 * Gets the value of the productTypeName property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getProductTypeName() {
		return productTypeName;
	}

	/**
	 * Sets the value of the productTypeName property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setProductTypeName(String value) {
		this.productTypeName = value;
	}

	/**
	 * Gets the value of the publisher property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 * Sets the value of the publisher property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setPublisher(String value) {
		this.publisher = value;
	}

	/**
	 * Gets the value of the regionCode property.
	 *
	 */
	public byte getRegionCode() {
		return regionCode;
	}

	/**
	 * Sets the value of the regionCode property.
	 *
	 */
	public void setRegionCode(byte value) {
		this.regionCode = value;
	}

	/**
	 * Gets the value of the releaseDate property.
	 *
	 * @return possible object is {@link XMLGregorianCalendar }
	 *
	 */
	public XMLGregorianCalendar getReleaseDate() {
		return releaseDate;
	}

	/**
	 * Sets the value of the releaseDate property.
	 *
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 *
	 */
	public void setReleaseDate(XMLGregorianCalendar value) {
		this.releaseDate = value;
	}

	/**
	 * Gets the value of the runningTime property.
	 *
	 * @return possible object is {@link RunningTime }
	 *
	 */
	public RunningTime getRunningTime() {
		return runningTime;
	}

	/**
	 * Sets the value of the runningTime property.
	 *
	 * @param value
	 *            allowed object is {@link RunningTime }
	 *
	 */
	public void setRunningTime(RunningTime value) {
		this.runningTime = value;
	}

	/**
	 * Gets the value of the studio property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getStudio() {
		return studio;
	}

	/**
	 * Sets the value of the studio property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setStudio(String value) {
		this.studio = value;
	}

	/**
	 * Gets the value of the title property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 *
	 * @param value
	 *            allowed object is {@link String }
	 *
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	public int subtractYear(String subject) {
		int year = -1;
		Pattern pattern = Pattern.compile("[0-9]{4}");
		Matcher matcher = pattern.matcher(subject);
		if (matcher.find()) {
			year = Integer.parseInt(matcher.group(0));
		}
		return year;
	}

	public int getYear() {
		return (getReleaseDate() != null) ? getReleaseDate().getYear() : -1;
	}

}