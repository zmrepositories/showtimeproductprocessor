package nl.hanze.movieshowtime.processor.external.amazon.entities;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LowestNewPrice")
public class LowestNewPrice
{


    private int amount;


    private String currencyCode;


    private String formattedPrice;

    /**
     * Gets the value of the amount property.
     *
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     */
    @XmlElement(name = "Amount")
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @XmlElement(name = "CurrencyCode")
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the formattedPrice property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFormattedPrice()
    {
        return formattedPrice;
    }

    /**
     * Sets the value of the formattedPrice property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @XmlElement(name = "FormattedPrice")
    public void setFormattedPrice(String value) {
        this.formattedPrice = value;
    }

}