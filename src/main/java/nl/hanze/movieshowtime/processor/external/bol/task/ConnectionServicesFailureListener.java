package nl.hanze.movieshowtime.processor.external.bol.task;

import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;

/**
 * listener will be fired if there can no connection been established to th services
 */
public interface ConnectionServicesFailureListener extends TaskListener
{
    /**
     *
     * @param task          - the task object
     * @param statusCode    - status response code from the ping functionality
     * @param sequence      - the sequence number of the ping
     */
    public void failed(Task task, int statusCode, int sequence);
}
