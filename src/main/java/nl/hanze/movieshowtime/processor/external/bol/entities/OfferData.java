package nl.hanze.movieshowtime.processor.external.bol.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * set field direct no need to get this data set only for testing
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="OfferData")
public class OfferData
{

    @XmlElement(name = "Offers")
    private Offer offer;


    @XmlElement(name = "NonProfessionalSellers")
    private String nonProfessionalSellers;


    @XmlElement(name = "ProfessionalSellers")
    private String professionalSellers;

    public Offer getOffer()
    {
        return offer;
    }
}
