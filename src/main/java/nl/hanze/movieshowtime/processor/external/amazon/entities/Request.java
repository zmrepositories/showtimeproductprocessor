package nl.hanze.movieshowtime.processor.external.amazon.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsValid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ItemSearchRequest">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Keywords" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ResponseGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SearchIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "isValid",
        "itemSearchRequest"
})
public class Request {

    @XmlElement(name = "IsValid", required = true)
    protected String isValid;
    @XmlElement(name = "ItemSearchRequest", required = true)
    protected ItemSearchRequest itemSearchRequest;

    /**
     * Gets the value of the isValid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIsValid() {
        return isValid;
    }

    /**
     * Sets the value of the isValid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIsValid(String value) {
        this.isValid = value;
    }

    /**
     * Gets the value of the itemSearchRequest property.
     *
     * @return
     *     possible object is
     *     {@link ItemSearchRequest }
     *
     */
    public ItemSearchRequest getItemSearchRequest() {
        return itemSearchRequest;
    }

    /**
     * Sets the value of the itemSearchRequest property.
     *
     * @param value
     *     allowed object is
     *     {@link ItemSearchRequest }
     *
     */
    public void setItemSearchRequest(ItemSearchRequest value) {
        this.itemSearchRequest = value;
    }


    

}