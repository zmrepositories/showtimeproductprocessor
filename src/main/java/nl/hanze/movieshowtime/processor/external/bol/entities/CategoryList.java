package nl.hanze.movieshowtime.processor.external.bol.entities;

import java.util.ArrayList;
import java.util.Iterator;


public class CategoryList extends ArrayList
{


    @SuppressWarnings("unchecked")
    public boolean contains(Category category)
    {
        boolean fount = false;
        Iterator<Category> iterator = iterator();

        while(iterator.hasNext() && !fount){

            fount = iterator.next().equals(category);
        }

        return fount;
    }


    public void addAll(CategoryPath path)
    {
        for (Category category : path.getCategories())
        {
            if(!contains(category)){
               add(category);
            }
        }

    }
}
