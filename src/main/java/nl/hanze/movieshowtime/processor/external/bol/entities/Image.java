package nl.hanze.movieshowtime.processor.external.bol.entities;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Images")
public class Image
{

    private String type;


    private String key;


    private String url;


    public String getKey()
    {
        return key;
    }


    @XmlElement(name = "Key")
    public Image setKey(String key)
    {
        this.key = key;
        return this;
    }


    public String getUrl()
    {
        return url;
    }


    @XmlElement(name = "Url")
    public Image setUrl(String url)
    {
        this.url = url;
        return this;
    }


    public String getType()
    {
        return type;
    }


    @XmlElement(name = "Type")
    public Image setType(String type)
    {
        this.type = type;
        return this;
    }


    public String toString()
    {
        return String.format("\n\t\t- Image: [\n " +
                "\t\t\t key: %s " +
                "\n\t\t\t type: %s "+
                "\n\t\t\t url: %s "+
                "\n\t\t]"
                ,getKey(), getType(), getUrl());
    }

}
