package nl.hanze.movieshowtime.processor.external.amazon;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import nl.hanze.movieshowtime.processor.external.amazon.entities.Item;
import nl.hanze.movieshowtime.processor.external.amazon.entities.ItemSearchResponse;
import nl.hanze.movieshowtime.processor.external.amazon.entities.Items;
import nl.hanze.movieshowtime.processor.external.amazon.task.SignedRequestsHelper;
import nl.hanze.movieshowtime.processor.external.amazon.task.URLHelper;

public class AmazonTest
{

	/**
	 * get list of all the amazon products back
	 *
	 * @return List amazon products
	 */
	public static void getAmazonProducts() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {

		// instantiate a new URLhelper
		URLHelper helper = new URLHelper(SignedRequestsHelper.getInstance("ecs.amazonaws.co.uk", "AKIAJP33LQB3NU7GACPQ", "9oWboam+IrQLMmIKEfptI12I27AKO1UIhUwFtC+P"));

		helper.setKeyWord("Blu-ray, dvd");
		helper.addParam("ItemPage", "1");
		helper.addParam("Version", "2013-08-01");


		// get a signed url from the URLhelper
		String signedUrl = helper.getSignedUrl();



		Client client = Client.create();
		// client.addFilter(new LoggingFilter(System.out));

		WebResource webResource = client.resource(signedUrl);


		System.out.println("DEBUG -- url: "+helper.getUrl());


		ClientResponse response = webResource.accept(MediaType.APPLICATION_XML).get(ClientResponse.class);

		if (response.getStatus() != 200) {
			// TODO logger and events!...

			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		ItemSearchResponse result = response
				.getEntity(ItemSearchResponse.class);

	
		Items items = result.getItems();
		List<Item> item = items.getItem();
		
		//test to print out all needed data for product
		for (int i = 0; i < item.size(); i++) {

			Item itemNext = item.get(i);


			System.out.println(itemNext);

		}

	}

	public static void main(String[] args) throws Exception {

		AmazonTest.getAmazonProducts();
	}

}
