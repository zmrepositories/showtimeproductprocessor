package nl.hanze.movieshowtime.processor.external.bol.task;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.client.ShowTimeClient;
import nl.hanze.movieshowtime.processor.external.bol.entities.SearchResults;
import nl.hanze.movieshowtime.processor.scheduler.Scheduler;
import nl.hanze.movieshowtime.processor.scheduler.Task;
import nl.hanze.movieshowtime.processor.scheduler.TaskListener;
import nl.hanze.movieshowtime.processor.utils.Log;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 *Class to create a specific bol task
 */
public class BolTask extends TimerTask implements Task
{

    /**
     * stored events for this task
     */
    private List<TaskListener> listeners = new ArrayList<TaskListener>();


    /**
     * the execute  or run increment number stored the count of executions
     */
    private long increment = 0L;


    /**
     * the name of the task
     */
    private String name = "BOL.com task runner";


    /**
     * default set on a interval sequence of five seconds for the task will run again
     */
    private long intervalTime = Scheduler.ONCE_PER_FIVE_SECONDS;


    /**
     * the number of max executions default disabled and will execute till there are no product to get
     * from de bol.com api
     */
    private long maxExecuteNumber = -1L;


    /**
     * the offset the start position of the set of products/movies/dvd to get from bol
     */
    private long offset = 0L;


    /**
     * limit or how many entries to retrieve from supplier default is set to on and wil save
     * one product at the time
     */
    private int limit = 1;


    /**
     * the stored api key this is been required by the developers bool.com api and must be embedded in the uri call
     * go to https://developers.bol.com/documentatie/open-api/aan-de-slag/ for more information
     */
    private String apiKey = "99E07659025C43AA97F3F59099DA7426";


    /**
     * default search value it will try to get all the dvd from bol.com
     */
    private String query = "dvd";


    /**
     * the bol dat com api version default its set to api version 4
     * https://developers.bol.com/documentatie/open-api/handleiding/open-api-versioning-policy/
     * https://developers.bol.com/nieuws/releasenotes/
     */
    private String version = "v4";


    /**
     * teh stored uri template will be filled with all the arguments
     */
    private String uri = "https://api.bol.com/catalog/%s/search?apikey=%s&q='%s'&offset=%s&limit=%s";


    /**
     * counter for empty results
     */
    private int countEmptyResult = 0;


    /**
     * max trigger by empty results
     */
    private int closeByEmptyResultMaxNumber = 10;


    private ShowTimeClient client;


    public BolTask(){

        this.client = new ShowTimeClient();
        // this.client.setDomainUrl("set here a different domain");
    }


    /**
     * constructor to set a task with other default of interval, offset and limit
     *
     * @param intervalTime long
     * @param offset int
     * @param limit int
     */
    public BolTask(long intervalTime, int offset, int limit)
    {
        setIntervalTime(intervalTime);
        setOffset(offset);
        setLimit(limit);
    }


    public long getIntervalTime()
    {
        return intervalTime;
    }


    /**
     * get the interval sequence value
     *
     * @param intervalTime long
     */
    public void setIntervalTime(long intervalTime)
    {
        this.intervalTime = intervalTime;
    }


    /**
     * increase the number of execution
     */
    private void increaseIncrementNumber()
    {
        if(increment < Long.MAX_VALUE)
            increment++;
        else
            increment=0;
    }


    /**
     * give back the increment number for this task
     *
     * @return increment long
     */
    public synchronized long getIncrementNumber()
    {
        return increment;
    }

    /**
     * getter for getting maxExecuteNumber
     *
     * @return long
     */
    public long getMaxExecuteNumber()
    {
        return maxExecuteNumber;
    }


    /**
     *  setter for maxExecuteNumber
     *
     * @param maxExecuteNumber long
     * @return Task
     */
    public Task setMaxExecuteNumber(long maxExecuteNumber)
    {
        this.maxExecuteNumber = maxExecuteNumber;
        return this;
    }

    /**
     * stop the task if the number is max execution is reached
     */
    private synchronized void cancelByMaxExecuteNumberReached()
    {
       if(maxExecuteNumber > -1 && maxExecuteNumber >= increment){
           cancel();
       }
    }


    /**
     * set the name of the task
     *
     * @param name name task
     */
    public synchronized void setName(String name)
    {
        this.name = name;
    }


    /**
     * get the name of the task
     *
     * @return name task
     */
    public synchronized String getName()
    {
        return name;
    }


    /**
     * getter for getting offset
     *
     * @return int
     */
    public long getOffset()
    {
        return offset;
    }


    /**
     *  setter for offset
     *
     * @param offset int
     */
    public synchronized BolTask setOffset(long offset)
    {
        this.offset     = offset;
        this.increment  = offset;
        return this;
    }


    /**
     * getter for getting limit
     *
     * @return int
     */
    public int getLimit()
    {
        return limit;
    }


    /**
     *  setter for limit,limitation of many products to load at on each api call
     *
     * @param limit int
     * @return void
     */
    public BolTask setLimit(int limit)
    {
        this.limit = limit;
        return this;
    }


    /**
     * getter for getting <code>apiKey</code> het the bol api key
     *
     * @return String
     */
    public String getApiKey()
    {
        return apiKey;
    }


    /**
     *  setter for <code>apiKey</code> store de bol developers api key
     *
     * @param api_key String
     * @return void
     */
    public BolTask setApiKey(String api_key)
    {
        this.apiKey = api_key;
        return this;
    }


    /**
     * getter for getting query
     *
     * @return String
     */
    public String getQuery()
    {
        return query;
    }


    /**
     *  setter for query
     *
     * @param query String
     * @return void
     */
    public BolTask setQuery(String query)
    {
        this.query = query;
        return this;
    }


    /**
     * getter for getting version
     *
     * @return String
     */
    public String getVersion()
    {
        return version;
    }


    /**
     *  setter for version
     *
     * @param version String
     * @return void
     */
    public BolTask setVersion(String version)
    {
        this.version = version;
        return this;
    }

    /**
     * getter for getting uri
     *
     * @return String
     */
    public synchronized String getURI()
    {
        return  String.format(uri, getVersion(), getApiKey(), getQuery(), getOffset(), getLimit());
    }


    /**
     * setter for uri
     *
     * @param  uri String
     * @return BolTask
     */
    public synchronized BolTask setURI(String uri)
    {
        this.uri = uri;
        return this;
    }


    /**
     * get list of all the bol products back
     *
     * @return List bol products
     */
    private synchronized List<nl.hanze.movieshowtime.processor.external.bol.entities.Product> getBolProducts()
    {
        List<nl.hanze.movieshowtime.processor.external.bol.entities.Product> products = new ArrayList<nl.hanze.movieshowtime.processor.external.bol.entities.Product>();
        try{

            Client client = Client.create();
            //client.addFilter(new LoggingFilter(System.out));

            WebResource webResource = client.resource(getURI());

            ClientResponse response = webResource.accept(MediaType.APPLICATION_XML).get(ClientResponse.class);


            if (response.getStatus() != 200)
            {
                // TODO logger and events!...

                //throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            SearchResults result = response.getEntity(SearchResults.class);

            if(result.getSize() > 0)
            {
                products.addAll(result.getProducts());
            }
        }catch(Exception e){

            e.printStackTrace();
            Log.getInstance().fatal(e.getMessage());
        }
        return products;
    }


    /**
     * the implementation of the task or job its self
     */
    @Override
    public void run()
    {
        try{
            cancelByMaxExecuteNumberReached();

            List<nl.hanze.movieshowtime.processor.external.bol.entities.Product> bolProducts = getBolProducts();

            int statusServices = client.doPing();

            if(statusServices == 200)
            {
                if(bolProducts.size() > 0){

                    for (nl.hanze.movieshowtime.processor.external.bol.entities.Product bolProduct : bolProducts){

                        nl.hanze.movieshowtime.entities.Product product = new nl.hanze.movieshowtime.entities.Product();

                        // create a new product to save
                        product
                                .setEan(bolProduct.getEAN())
                                .setProductId( Long.toString(bolProduct.getId()) )
                                .setTitle(bolProduct.getTitle())
                                .setGpc(bolProduct.getGPC())
                                .setAvailabilityCode(bolProduct.getOffer().getAvailabilityCode())
                                .setAvailabilityDescription(bolProduct.getOffer().getAvailabilityDescription())
                                .setCondition(bolProduct.getOffer().getCondition())
                                .setPrice(bolProduct.getOffer().getPrice())
                                .setOrigin(bolProduct.getOffer().getSeller().getType())
                                .setYear(bolProduct.getYear());

                        productAddNotify( product, client.getResponse());

                        // save and notify events
                        if(client.addProduct( product ))
                        {
                            successNotify(client.getResponse(), product, client.getStatus());
                        }else{
                            failedNotify(client.getResponse(), product, client.getStatus());
                        }

                    }

                }else{
                    if(countEmptyResult <= closeByEmptyResultMaxNumber){
                        emptyResultNotify();
                        countEmptyResult++;
                    }else{
                        // kill task
                        cancel();
                        Log.getInstance().notice(String.format("STOP task:[%s] is stopped", getName()));
                    }
                }
            }else{
                // if by a sequence of four times ping the services is not available stp the task
               if(getIncrementNumber() >=3 ){
                   cancel();
                   Log.getInstance().notice(String.format("STOP task:[%s] is stopped", getName()));
               }
                connectServicesFailedNotify(statusServices, (int) getIncrementNumber()+1);
            }


            increaseIncrementNumber();

            if(statusServices == 200)
            {
                setOffset( getIncrementNumber() );
            }
        }catch(Exception e){

            e.printStackTrace();
            Log.getInstance().fatal(e.getMessage());
            // skip and go further...
            increaseIncrementNumber();
            setOffset( getIncrementNumber() );
        }
    }


    /**
     * get all the subscribed listeners
     *
     * @return List<TaskListener>
     */
    public synchronized List<TaskListener> getListeners()
    {
        return listeners;
    }


    /**
     * add a listener
     *
     * @param listener TaskListener
     */
    public synchronized void addListener(TaskListener listener)
    {
        getListeners().add(listener);
    }


    /**
     * file all the task events
     */
    public synchronized void emptyResultNotify()
    {
        for(TaskListener listener : getListeners()){

            if(listener instanceof EmptyResultListener)
            {
                ((EmptyResultListener)listener).actionPerformed(this);
            }
        }
    }


    /**
     * file all the task events
     */
    public synchronized void productAddNotify(nl.hanze.movieshowtime.entities.Product product, Message response)
    {
        for(TaskListener listener : getListeners()){

            if(listener instanceof ProductAddListener)
            {
                ((ProductAddListener)listener).actionPerformed( this, product, response);
            }
        }
    }


    /**
     * notify on failed saving product
     */
    public synchronized void failedNotify(Message response, nl.hanze.movieshowtime.entities.Product product, int statusCode)
    {
        for(TaskListener listener : getListeners())
        {
            if(listener instanceof FailedListener)
            {
                ((FailedListener)listener).failed( this, response, product, statusCode);
            }
        }
    }


    /**
     * notify on failed saving product
     */
    public synchronized void connectServicesFailedNotify(int statusCode, int sequence)
    {
        for(TaskListener listener : getListeners())
        {
            if(listener instanceof ConnectionServicesFailureListener)
            {
                ((ConnectionServicesFailureListener)listener).failed( this, statusCode, sequence);
            }
        }
    }



    /**
     * notify on successfully product saved
     */
    public synchronized void successNotify(Message response, nl.hanze.movieshowtime.entities.Product product, int statusCode)
    {
        for(TaskListener listener : getListeners())
        {
            if(listener instanceof SuccessListener)
            {
                ((SuccessListener)listener).success(this, response, product, statusCode);
            }
        }
    }


    /**
     * get the show time client from the task
     *
     * @return ShowTimeClient
     */
    public ShowTimeClient getClient()
    {
        return client;
    }
}
