package nl.hanze.movieshowtime.processor.scheduler;


public interface TaskListener
{
    public void actionPerformed(Task task);
}
