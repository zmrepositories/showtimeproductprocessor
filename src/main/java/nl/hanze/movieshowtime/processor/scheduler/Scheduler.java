package nl.hanze.movieshowtime.processor.scheduler;

import java.util.*;


public class Scheduler extends Timer
{

    /**
     * constant for set a interval by once per minutes
     */
    public final static long ONCE_PER_MINUTES = 1000*60;


    /**
     *  time interval static field seconds on interval of file seconds
     */
    public final static long ONCE_PER_TEN_SECONDS = 10000L;


    /**
     *  time interval static field seconds on interval of file seconds
     */
    public final static long ONCE_PER_FIVE_SECONDS = 5000L;


    /**
     *  time interval static field seconds on interval of file seconds
     */
    public final static long ONCE_PER_THREE_SECONDS = 3000L;


    /**
     *  time interval static field seconds on interval of file seconds
     */
    public final static long ONCE_PER_TWO_SECONDS = 2000L;


    /**
     * stored or added tasks
     */
    private List<Task> tasks;



    public Scheduler()
    {
        tasks = new ArrayList<Task>();
    }


    /**
     * get all stored tasks
     *
     * @return List<Task>
     */
    public List<Task> getTasks(){
        return tasks;
    }


    /**
     * add a single new task
     * @param task Task
     */
    public void addTask(Task task)
    {
        tasks.add(task);
    }


    /**
     * execute all the submit tasks
     */
    public void execute()
    {
        for(Task task : tasks)
        {
            System.out.println(String.format("INFO adding task:[%s]", task.getName()) );

            this.scheduleAtFixedRate((TimerTask) task, new Date(), task.getIntervalTime());
        }
    }


    /**
     * close all the tasks
     */
    public void close(){
        for(Task task : tasks)
        {
            System.out.println(String.format("INFO closing task:[%s]", task.getName()));

            task.cancel();
        }
    }


    /**
     * remove a task from the list and deactivate task
     *
     * @param task Task
     */
    public void removeTask(SampleTask task)
    {
        if(task != null){
            for(Task entry : tasks){
                if(task.equals(entry))
                {
                    String name = task.getName();
                    task.cancel();
                    tasks.remove(task);

                    System.out.println(String.format("INFO removed task:[%s]", name));
                }
            }
        }

    }


    /**
     * close and empty list
     */
    public void clear(){
        close();
        tasks.clear();
    }


    /**
     * get a task by name
     *
     * @param name string
     * @return Task
     */
    public Task getTaskByName(String name)
    {
        boolean fount   = false;
        Task task       = null;
        Iterator i      = tasks.iterator();

        while(!fount && i.hasNext())
        {
            Task temp = (Task)i.next();
            if(temp.getName().equals(name) )
            {
                fount = true;
                task = temp;
            }
        }
        return task;
    }


    public String toString()
    {
        String message = String.format("running (%s) tasks", getTasks().size());
        for(Task task : tasks){
            message +=  task.toString();
        }
        return message;
    }


    /**
     * sample how to use the scheduler
     *
     * @param args arguments
     */
    public static void main(String[] args)
    {

        Scheduler scheduler = new Scheduler();


        Task task1 = new SampleTask("test 1", Scheduler.ONCE_PER_FIVE_SECONDS);
        task1.addListener(new TaskListener() {
            @Override
            public void actionPerformed(Task task)
            {
                System.out.println(String.format(".%s Run Sample named:[%s] now on:[%s]",task.getIncrementNumber(), task.getName(), new Date()) );
            }
        });


        Task task2 = new SampleTask("test 2", Scheduler.ONCE_PER_TWO_SECONDS);
        task2.addListener(new TaskListener() {
            @Override
            public void actionPerformed(Task task)
            {
                System.out.println(String.format(".%s Run Sample named:[%s] now on:[%s]",task.getIncrementNumber(), task.getName(), new Date()) );
            }
        });

        scheduler.addTask(task1);
        scheduler.addTask(task2);


        scheduler.execute();
    }

}
