package nl.hanze.movieshowtime.processor.scheduler;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class SampleTask extends TimerTask implements Task
{

    private List<TaskListener> listeners = new ArrayList<TaskListener>();


    private long increment = 0L;

    /**
     * the name of the task
     */
    private String name;


    private long intervalTime;


    public long getIntervalTime()
    {
        return intervalTime;
    }


    public void setIntervalTime(long intervalTime)
    {
        this.intervalTime = intervalTime;
    }


    /**
     * add a with a listener as second arguments
     * @param name String
     */
    public SampleTask(String name)
    {
        setName(name);
    }


    public SampleTask(String name, long intervalTime)
    {
        setName(name);
        setIntervalTime(intervalTime);
    }


    /**
     * set the name of the task
     *
     * @param name name task
     */
    public synchronized void setName(String name)
    {
        this.name = name;
    }


    /**
     * get the name of the task
     *
     * @return name task
     */
    public synchronized String getName()
    {
        return name;
    }


    /**
     * increase the number of execution
     */
    private void increaseIncrementNumber()
    {
        if(increment < Long.MAX_VALUE)
            increment++;
        else
            increment=0;
    }


    /**
     * give back the increment number for this task
     *
     * @return increment long
     */
    public long getIncrementNumber()
    {
        return increment;
    }


    @Override
    public void run()
    {
        increaseIncrementNumber();

        actionPerformed();
    }


    /**
     * get all the subscribed listeners
     *
     * @return List<TaskListener>
     */
    public synchronized List<TaskListener> getListeners()
    {
        return listeners;
    }


    /**
     * add a listener
     *
     * @param listener TaskListener
     */
    public synchronized void addListener(TaskListener listener)
    {
        getListeners().add(listener);
    }


    /**
     * file all the task events
     */
    public synchronized void actionPerformed()
    {
        for(TaskListener listener : getListeners()){
            listener.actionPerformed( this );
        }
    }
}