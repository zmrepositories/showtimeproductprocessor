package nl.hanze.movieshowtime.processor.scheduler;


import java.util.List;

/**
 * @version 1.0
 *
 * abstract task need to have the volleying methods
 */
public interface Task
{

    /**
     * add a with a listener as second arguments
     *
     * @param name String
     */
    public void setName(String name);


    /**
     * get the name of the task
     *
     * @return name task
     */
    public String getName();


    /**
     * give back the number of execution of the task, it tels how many the task is been executed
     * @return long number/count
     */
    public long getIncrementNumber();


    /**
     * get the interval sequence value
     *
     * @return long interval
     */
    public long getIntervalTime();


    /**
     * get the interval sequence value
     *
     * @return long interval
     */
    public void setIntervalTime(long intervalTime);



    /**
     * cancel the task
     * @return boolean is been canceled
     */
    public boolean cancel();




    /**
     * get all the subscribed listeners
     *
     */
     public List<TaskListener> getListeners();


    /**
     * add a listener
     *
     * @param listener TaskListener
     */
    public void addListener(TaskListener listener);


}
