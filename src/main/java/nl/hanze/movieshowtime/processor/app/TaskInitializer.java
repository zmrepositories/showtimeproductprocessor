package nl.hanze.movieshowtime.processor.app;


import nl.hanze.movieshowtime.processor.scheduler.Task;

public interface TaskInitializer
{
    /**
     * this method will be called when the tasked is been initialized
     */
    public void load();


    /**
     * required method for getting the task
     *
     * @return Task
     */
    public Task getTask();


    /**
     * needed for getting the services specific configuration settings
     * @param settings ServicesSetting
     */
    public void setServicesSettings(ServicesSetting settings);
}
