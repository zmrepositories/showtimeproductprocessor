package nl.hanze.movieshowtime.processor.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="application")
@XmlAccessorType(XmlAccessType.FIELD)
public class Model
{
    public final static String CONFIG = System.getProperty("user.dir")+"/config/config.xml";

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "version")
    private float version;

    @XmlElement(name = "importSettings")
    private ImportSetting settings;

    /**
     * getter for getting name
     *
     * @return String
     */
    public String getName()
    {
        return name;
    }


    /**
     *  setter for name
     *
     * @param name String
     * @return Model
     */
    public Model setName(String name)
    {
        this.name = name;
        return this;
    }


    /**
     * getter for getting version
     *
     * @return float
     */
    public float getVersion()
    {
        return version;
    }


    /**
     *  setter for version
     *
     * @param version loat
     * @return Model
     */
    public Model setVersion(float version)
    {
        this.version = version;
        return this;
    }

    /**
     * getter for the settings
     *
     * @return the import settings
     */
    public ImportSetting getSettings()
    {
        return settings;
    }

}
