package nl.hanze.movieshowtime.processor.app;

import nl.hanze.movieshowtime.processor.scheduler.Task;

import javax.xml.bind.annotation.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


@XmlRootElement(name="task")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskModule
{
    @XmlElement(name = "name")
    private String name;


    @XmlElement(name = "interval")
    private long interval = -1L;


    @XmlElement(name = "offset")
    private long offset = 0L;


    @XmlElement(name = "maxExecution")
    private long maxExecuteNumber = 0L;


    @XmlElement(name = "class")
    private String classPath;


    @XmlElement(name = "loadClass")
    private String loadClassPath;


    @XmlElement(name = "publicKey")
    private String publicKey;


    @XmlElement(name = "privateKey")
    private String privateKey;


    @XmlElement(name = "queryValue")
    private String queryValue;

    @XmlElement(name = "apiVersion")
    private String apiVersion;


    /**
     * is the task enabled or not, if its enabled it will initialize the task object
     */
    @XmlAttribute(name = "enabled")
    private boolean isEnabled = false;




    @XmlTransient
    private Task task;


    @XmlTransient
    private TaskInitializer initializer;



    /**
     * create the task if there is a classPath and its allowed to
     */
    public void create() throws ClassNotFoundException, InstantiationException, IllegalAccessException , NoSuchMethodException, InvocationTargetException
    {
       if(isTaskEnabled() && !classPath.isEmpty())
       {
           System.out.println("yes create: "+classPath);
           task = (Task) Class.forName(classPath).newInstance();

           task.setName(name);

           if(interval > -1L)
            task.setIntervalTime(interval);

           if(!loadClassPath.isEmpty())
           {
              
			@SuppressWarnings("rawtypes")
			Constructor constructor = Class.forName(loadClassPath).getConstructor(TaskModule.class);
               initializer = (TaskInitializer) constructor.newInstance( this );
           }


       }
    }


    /**
     * getter for getting name
     *
     * @return String
     */
    public String getName()
    {
        return name;
    }


    /**
     *  setter for name
     *
     * @param name String
     * @return TaskModule
     */
    public TaskModule setName(String name)
    {
        this.name = name;
        return this;
    }


    /**
     * getter for getting interval
     *
     * @return long
     */
    public long getInterval()
    {
        return interval;
    }


    /**
     *  setter for interval
     *
     * @param interval long
     * @return TaskModule
     */
    public TaskModule setInterval(long interval)
    {
        this.interval = interval;
        return this;
    }

    /**
     * getter for getting offset
     *
     * @return long
     */
    public long getOffset()
    {
        return offset;
    }


    /**
     *  setter for offset
     *
     * @param offset long
     * @return TaskModule
     */
    public TaskModule setOffset(long offset)
    {
        this.offset = offset;
        return this;
    }


    /**
     * getter for getting maxExecuteNumber
     *
     * @return long
     */
    public long getMaxExecuteNumber()
    {
        return maxExecuteNumber;
    }


    /**
     *  setter for maxExecuteNumber
     *
     * @param maxExecuteNumber long
     * @return BolTask
     */
    public TaskModule setMaxExecuteNumber(long maxExecuteNumber)
    {
        this.maxExecuteNumber = maxExecuteNumber;
        return this;
    }


    /**
     * give back the task object based on the configuration
     *
     * @return Task
     */
    public Task getTask()
    {
        return task;
    }

    /**
     * whether it has a load/initialize class or not
     * @return boolean
     */
    public boolean hasLoadClass()
    {
        return (initializer != null);
    }

    /**
     * give back the load class for the task
     *
     * @return TaskInitializer
     */
    public TaskInitializer getLoadClass()
    {
        return initializer;
    }


    /**
     * is the task been enabled or not
     *
     * @return boolean enabled
     */
    public boolean isTaskEnabled()
    {
        return isEnabled;
    }

    /**
     * give back the public key api
     *
     * @return public key
     */
    public String getPublicKey()
    {
        return publicKey;
    }

    /**
     * give back the private key api
     *
     * @return private key
     */
    public String getPrivateKey()
    {
        return privateKey;
    }

    /**
     * give back the query search string or keyword to search with...
     *
     * @return String query / keyword
     */
    public String getQueryValue()
    {
        return queryValue;
    }

    /**
     * give back the query search string or keyword to search with...
     *
     * @return String query / keyword
     */
    public String getApiVersion()
    {
        return apiVersion;
    }

}
