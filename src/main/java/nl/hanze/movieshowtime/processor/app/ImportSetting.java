package nl.hanze.movieshowtime.processor.app;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "importSettings")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImportSetting {

	@XmlElementWrapper(name = "taskModules")
	@XmlElement(name = "task")
	private List<TaskModule> taskSettings;

	@XmlElement(name = "services")
	private ServicesSetting servicesSettings;

	/**
	 * give back all the task settings
	 * 
	 * @return List
	 */
	public List<TaskModule> getTaskModules() {
		return taskSettings;
	}

	public ServicesSetting getServicesSettings() {
		return servicesSettings;
	}

}
