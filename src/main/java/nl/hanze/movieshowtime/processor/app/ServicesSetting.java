package nl.hanze.movieshowtime.processor.app;

import nl.hanze.movieshowtime.processor.utils.Utils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="services")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServicesSetting
{

    @XmlElement(name = "host")
    private String host;


    @XmlElement(name = "port")
    private int port;

    /**
     * getter for getting host
     *
     * @return String
     */
    public String getHost()
    {
        return host;
    }


    /**
     *  setter for host
     *
     * @param host String
     * @return ServicesSetting
     */
    public ServicesSetting setHost(String host)
    {
        this.host = host;
        return this;
    }


    /**
     * getter for getting port
     *
     * @return int portnumber
     */
    public int getPort()
    {
        return port;
    }


    /**
     *  setter for port
     *
     * @param port int portnumber
     * @return ServicesSetting
     */
    public ServicesSetting setPort(int port)
    {
        this.port = port;
        return this;
    }


    /**
     * get the services url for the task to save the data/products
     *
     * @return String url
     */
    public String getServicesURL()
    {
        return Utils.fixPath(String.format("%s:%s", host, port), "/");
    }

}
