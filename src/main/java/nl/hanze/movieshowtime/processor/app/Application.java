package nl.hanze.movieshowtime.processor.app;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import nl.hanze.movieshowtime.processor.scheduler.Scheduler;

public class Application {
	/**
	 * stored the application model
	 */
	private Model model;

	private Scheduler scheduler;

	/**
	 * constructor initialize the application
	 */
	public Application() {
		// create a new scheduler
		scheduler = new Scheduler();

		try {
			init();

			for (TaskModule module : getModel().getSettings().getTaskModules()) {

				if (module.isTaskEnabled()) {
					module.create();
					if (module.hasLoadClass()) {
						// set services settings...
						module.getLoadClass().setServicesSettings(
								model.getSettings().getServicesSettings());
						// load the class initialize it ...
						module.getLoadClass().load();
						// get the task...
						scheduler.addTask(module.getLoadClass().getTask());
					} else {

						scheduler.addTask(module.getTask());
					}
				}

			}
		} catch (JAXBException | IOException | ClassNotFoundException
				| InstantiationException | IllegalAccessException
				| NoSuchMethodException | InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	/**
	 * load application settings like model etc
	 *
	 * @throws JAXBException
	 * @throws IOException
	 */
	private void init() throws JAXBException, IOException {
		Unmarshaller unmarshaller = JAXBContext.newInstance(Model.class)
				.createUnmarshaller();
		model = (Model) unmarshaller.unmarshal(new FileReader(Model.CONFIG));
	}

	public void run() {
		scheduler.execute();
	}

	/**
	 * getter for getting model
	 *
	 * @return Model
	 */
	public Model getModel() {
		return model;
	}
}
