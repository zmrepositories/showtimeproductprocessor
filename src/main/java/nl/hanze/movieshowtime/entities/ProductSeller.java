package nl.hanze.movieshowtime.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;


@Entity
@Table(name = "product_seller", schema = "", catalog = "showtimeproducts")
@XmlAccessorType(XmlAccessType.NONE)
public class ProductSeller implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;



    @Id
    @Column(name = "product_id")
    private long productId;


    @Id
    @Column(name = "seller_id")
    private long sellerId;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="seller_id", referencedColumnName="id")
    private Seller seller;



    @ManyToOne
    @PrimaryKeyJoinColumn(name="product_id", referencedColumnName="id")
    private Product product;



    @XmlElement(name = "seller")
    public Seller getSeller(){
        return seller;
    }


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public void setProductId(long productId)
    {
        this.productId = productId;
    }


    public long getProductId() {
        return productId;
    }


    public void setSellerId(long sellerId)
    {
        this.sellerId = sellerId;
    }


    public long getSellerId()
    {
        return sellerId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductSeller that = (ProductSeller) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(((Long)id).toString());
    }
}
