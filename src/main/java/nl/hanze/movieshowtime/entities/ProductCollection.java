package nl.hanze.movieshowtime.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="products")
public class ProductCollection
{

    @XmlElement(name="product")
    private List<Product> products;


    public ProductCollection()
    {
        products = new ArrayList<Product>();
    }


    /**
     * getter for getting products
     *
     * @return List<Product>
     */
    @XmlElement(name="product")
    public List<Product> getAll()
    {
        return products;
    }


    /**
     *  setter for persons
     *
     * @param products List<Product>
     * @return PersonCollection
     */
    public ProductCollection addAll(List<Product> products)
    {
        this.products.addAll(products);
        return this;
    }

    public Product get(int index)
    {
        return products.get(index);
    }



    public ProductCollection add(Product product)
    {
        products.add(product);
        return this;
    }


    public void splice(int start, int end)
    {
        products = products.subList(start, end);
    }


    public String toString()
    {
        String out = "Collection:[\n";
        for(Product product : getAll())
        {
            out += "\n  "+product.toString();
        }
        out += "\n];";
        return out;
    }
}
