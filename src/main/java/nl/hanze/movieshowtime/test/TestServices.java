package nl.hanze.movieshowtime.test;

import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import nl.hanze.movieshowtime.client.ShowTimeClient;
import nl.hanze.movieshowtime.entities.Product;

import org.junit.Test;

public class TestServices {

	/**
	 * Test of de response statuscode 200 ok is url:
	 * showtime/services/product/id/1
	 */
	@Test
	public void testGetProductById() {

		ShowTimeClient client = new ShowTimeClient();

		client.getProductById(1L);

		assertTrue(client.getStatus() == 200);

	}

	/**
	 * Test of de response statuscode 200 ok is url:
	 * showtime/services/product/ean/8712609653694
	 */
	@Test
	public void testGetProductByEAN() {

		ShowTimeClient client = new ShowTimeClient();

		client.getProductByEan(8712609653694L);

		assertTrue(client.getStatus() == 200);

	}

	/**
	 * Test of de response statuscode 200 ok is url:
	 * showtime/services/product/title/avonturenvankuifje
	 * 
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void testGetProductByTitle() throws UnsupportedEncodingException {

		ShowTimeClient client = new ShowTimeClient();

		client.searchProductByTitle("Avonturen Van Kuifje");

		assertTrue(client.getStatus() == 200);

	}

	/**
	 * Test of de response statuscode 200 ok is url:
	 * showtime/services/product/title/harrypotter/2014
	 * 
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void testGetProductByTitleAndYear()
			throws UnsupportedEncodingException {

		ShowTimeClient client = new ShowTimeClient();

		client.searchProductByTitleAndYear("Harry Potter", 2014);

		assertTrue(client.getStatus() == 200);

	}

	/**
	 * Test of er een product toegevoegd kan worden url:
	 * showtime/services/product
	 */
	@Test
	public void testAddProduct() {

		ShowTimeClient client = new ShowTimeClient();

		Product product = new Product();

		product.setEan(9782203003040L).setProductId("2344345")
				.setTitle("Tintin Au Congo").setGpc("book")
				.setAvailabilityCode(175)
				.setAvailabilityDescription("3-5 werkdagen")
				.setCondition("Nieuw").setPrice(new BigDecimal(9.99))
				.setYear(2012);

		assertTrue(client.addProduct(product));

		assertTrue(client.getStatus() == 201);

	}

	/**
	 * Test of bij het toevoegen van een product de responsecode 201 created is
	 * url: showtime/services/product
	 */
	@Test
	public void testAddProductStatus() {

		ShowTimeClient client = new ShowTimeClient();

		Product product = new Product();

		product.setEan(9782203003040L).setProductId("2344345")
				.setTitle("Tintin Au Congo").setGpc("book")
				.setAvailabilityCode(175)
				.setAvailabilityDescription("3-5 werkdagen")
				.setCondition("Nieuw").setPrice(new BigDecimal(9.99))
				.setYear(2012);

		client.addProduct(product);

		assertTrue(client.getStatus() == 201);

	}

}
