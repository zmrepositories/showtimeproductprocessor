package nl.hanze.movieshowtime.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestApis.class, TestServices.class })
public class AllTests {

}
