package nl.hanze.movieshowtime.test;

import static org.junit.Assert.assertNotNull;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.core.MediaType;

import nl.hanze.movieshowtime.processor.external.amazon.entities.ItemSearchResponse;
import nl.hanze.movieshowtime.processor.external.amazon.task.SignedRequestsHelper;
import nl.hanze.movieshowtime.processor.external.amazon.task.URLHelper;
import nl.hanze.movieshowtime.processor.external.bol.entities.SearchResults;

import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class TestApis {

	/**
	 * Test om te kijken of er een result uit de bol api komt. fail als er het
	 * result null is
	 */
	@Test
	public void testBol() {

		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));

		WebResource webResource = client
				.resource("https://api.bol.com/catalog/v4/search?apikey=99E07659025C43AA97F3F59099DA7426&q='dvd'&offset=300000&limit=5");

		ClientResponse response = webResource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());

		}

		SearchResults result = response.getEntity(SearchResults.class);
		assertNotNull(result);

	}

	/**
	 * Test om te kijken of er een result uit de amazon api komt. fail als er
	 * het result null is
	 */

	@Test
	public void testAmazon() throws InvalidKeyException,
			IllegalArgumentException, UnsupportedEncodingException,
			NoSuchAlgorithmException {

		URLHelper helper = new URLHelper(SignedRequestsHelper.getInstance(
				"ecs.amazonaws.co.uk", "AKIAJP33LQB3NU7GACPQ",
				"9oWboam+IrQLMmIKEfptI12I27AKO1UIhUwFtC+P"));

		helper.setKeyWord("dvd");
		helper.addParam("ItemPage", "1");
		helper.addParam("Version", "2013-08-01");

		String signedUrl = helper.getSignedUrl();

		Client client = Client.create();
		WebResource webResource = client.resource(signedUrl);

		ClientResponse response = webResource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {

			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		ItemSearchResponse result = response
				.getEntity(ItemSearchResponse.class);
		assertNotNull(result);

	}

}
