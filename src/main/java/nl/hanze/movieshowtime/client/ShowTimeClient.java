package nl.hanze.movieshowtime.client;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.core.MediaType;

import nl.hanze.movieshowtime.client.response.Message;
import nl.hanze.movieshowtime.entities.Product;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import nl.hanze.movieshowtime.entities.ProductCollection;

/**
 * custom client for movie show time product
 *
 * @version 1.0
 * @author Michel Tartarotti
 */
public class ShowTimeClient {

	private String domainUrl = "http://localhost:8080/";

	/**
	 * POST, PUT url for put post products
	 */
	public static final String PRODUCT_URI = "showtime/services/product";

	/**
	 * GET the test url
	 */
	public static final String TEST_URI = "showtime/services/test";

	/**
	 * GET,POST url for getting or creating all products
	 */
	public static final String PRODUCTS_URI = "showtime/services/products";

	/**
	 * GET, DELETE get product by id
	 */
	public static final String GET_PRODUCT_ID_URI = "showtime/services/product/id/%s";

	/**
	 * GET, DELETE get product by ean or delete by ean
	 */
	public static final String GET_PRODUCT_EAN_URI = "showtime/services/product/ean/%s";

	/**
	 * GET search for a single product by title
	 */
	public static final String SEARCH_PRODUCT_URI = "showtime/services/product/search/";

	/**
	 * GET search for products by title
	 */
	public static final String SEARCH_PRODUCTS_URI = "showtime/services/products/search/";

	private Message message;

	private int status;

	Client client;

	public ShowTimeClient() {
		client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
	}

	/**
	 * set domain url
	 * 
	 * @param url
	 */
	public void setDomainUrl(String url) {
		this.domainUrl = url;
	}

	/**
	 * get domain url
	 *
	 * @return url domain
	 */
	public String getDomainUrl() {
		return this.domainUrl;
	}

	/**
	 * get message response
	 *
	 * @return Message
	 */
	public Message getResponse() {
		return message;
	}

	/**
	 * get the current request status
	 *
	 * @return int status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * helper method fix uri entities problem
	 * 
	 * @param s
	 *            String
	 * @return url
	 */
	public static String encodeURIComponent(String s) {
		String result;

		try {
			result = URLEncoder.encode(s, "UTF-8").replaceAll("\\+", "%20")
					.replaceAll("\\%21", "!").replaceAll("\\%27", "'")
					.replaceAll("\\%28", "(").replaceAll("\\%29", ")")
					.replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			result = s;
		}

		return result;
	}

	/**
	 * do a simple single one ping on domain for checking if its alive or not
	 *
	 * @return int status int
	 */
	public int doPing() {

		int status = -1;

		try {
			URL siteURL = new URL(this.domainUrl + TEST_URI);
			HttpURLConnection connection = (HttpURLConnection) siteURL
					.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			status = connection.getResponseCode();

		} catch (Exception e) {
			status = 404;
		}
		return status;
	}

	/**
	 * retrieve all products
	 *
	 * @return List<Product>
	 */
	public List<Product> getAllProducts() {
		WebResource resource = client.resource(this.domainUrl
				+ PRODUCTS_URI);

		return resource.accept(MediaType.APPLICATION_XML).get(
				new GenericType<List<Product>>() {
				});
	}

	/**
	 * get get a product by id
	 *
	 * @param id
	 *            long
	 * @return Product
	 */
	public Product getProductById(long id) {
		Product product = null;

		WebResource resource = client.resource(this.domainUrl
				+ String.format(GET_PRODUCT_ID_URI, id));

		ClientResponse response = resource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		status = response.getStatus();
		if (status != 200) {
			message = response.getEntity(Message.class);
		} else {
			product = response.getEntity(Product.class);
		}

		return product;
	}

	/**
	 * get get a product by ean
	 *
	 * @param ean
	 *            long
	 * @return Product
	 */
	public Product getProductByEan(long ean) {
		Product product = null;

		WebResource resource = client.resource(this.domainUrl
				+ String.format(GET_PRODUCT_EAN_URI, ean));

		ClientResponse response = resource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		status = response.getStatus();
		if (status != 200) {
			message = response.getEntity(Message.class);
		} else {
			product = response.getEntity(Product.class);
		}

		return product;
	}

	/**
	 * search a product by title
	 *
	 * @param title
	 *            String
	 * @return Product
	 */
	public Product searchProductByTitle(String title)
			throws UnsupportedEncodingException {
		Product product = null;

		WebResource resource = client
				.resource(this.domainUrl + SEARCH_PRODUCT_URI
                        + ShowTimeClient.encodeURIComponent(title));

		ClientResponse response = resource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		status = response.getStatus();
		if (status != 200) {
			message = response.getEntity(Message.class);
		} else {
			product = response.getEntity(Product.class);
		}

		return product;
	}

	/**
	 * search a product by title and year
	 *
	 * @param title
	 *            String
	 * @return Product
	 */
	public Product searchProductByTitleAndYear(String title, int year)
			throws UnsupportedEncodingException {
		Product product = null;

		WebResource resource = client.resource(this.domainUrl
                + SEARCH_PRODUCT_URI + ShowTimeClient.encodeURIComponent(title)
                + "/" + year);

		ClientResponse response = resource.accept(MediaType.APPLICATION_XML)
				.get(ClientResponse.class);

		status = response.getStatus();
		if (status != 200) {
			message = response.getEntity(Message.class);
		} else {
			product = response.getEntity(Product.class);
		}

		return product;
	}

	/**
	 * add a single new product
	 *
	 * @param product
	 *            Product
	 * @return Product
	 */
	public boolean addProduct(Product product) {

		WebResource resource = client.resource(this.domainUrl + PRODUCT_URI);

		ClientResponse response = resource.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML)
				.post(ClientResponse.class, product);

		status = response.getStatus();

		message = response.getEntity(Message.class);

		return status == 201;
	}

    /**
     * add multiple new product, sed and retrieve json to reduce the payload
     * notice importent temporary fix header product size
     *
     * @param products ProductCollection
     * @return boolean status
     */
    public boolean addProducts(ProductCollection products)
    {
        WebResource resource = client.resource(this.domainUrl+PRODUCTS_URI);

        ClientResponse response = resource
                .header("Product-size", products.getAll().size())
                .type(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, products);

        status  = response.getStatus();

        message = response.getEntity(Message.class);

        return status == 201;
    }

	/**
	 * update a single new product
	 *
	 * @param product
	 *            Product
	 * @return Product
	 */
	public boolean updateProduct(Product product) {

		WebResource resource = client.resource(this.domainUrl + PRODUCT_URI);

		ClientResponse response = resource.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML)
				.put(ClientResponse.class, product);

		status = response.getStatus();

		message = response.getEntity(Message.class);

		return status == 201;
	}

	/**
	 * update a single new product
	 *
	 * @param ean
	 *            long
	 * @return Product
	 */
	public boolean deleteProductByEan(long ean) {

		WebResource resource = client.resource(this.domainUrl
				+ GET_PRODUCT_EAN_URI);

		ClientResponse response = resource.accept(MediaType.APPLICATION_XML)
				.delete(ClientResponse.class, ean);

		status = response.getStatus();

		message = response.getEntity(Message.class);

		return status == 201;
	}
}
