package nl.hanze.movieshowtime.client.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="fault")
public class Fault
{
    @XmlElement(name="responseCode")
    private int code;

    @XmlElement(name="responseMessage")
    private String message;

    @XmlElement(name="product")
    private Item item;

    public Fault(){}

    public Fault(int code, String message)
    {
        this();
        this.code    = code;
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public int getCode()
    {
        return code;
    }

    public Fault setItem(Item item)
    {
        this.item = item;
        return this;
    }

    public Item getItem()
    {
        return item;
    }

    public String toString()
    {
        return String.format("\n\tFault: [\n " +
                "\t  code: %s " +
                "\n\t  message: %s " +
                "%s"+
                "\n\t]",code, message, ((item == null)?   "": item));
    }
}
