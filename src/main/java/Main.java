import nl.hanze.movieshowtime.processor.app.Application;
/*
 * Main Class to run the whole application. Makes a new application instance and call the run method 
 */
public class Main
{
    public static void main(String[] args)
    {

        Application app = new Application();
        app.run();
    }
}
